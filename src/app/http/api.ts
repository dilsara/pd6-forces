import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class API {

    BASE_PATH = "http://sathkara.pensions.gov.lk:8080/pensions_api_v2";
    MASTER_DATA_BASE_PATH = 'http://sathkara.pensions.gov.lk:8080/pms_masterdata_api/api';
    // BASE_PATH1  ='http://localhost:8080/pms-api/api';
    BASE_PATH1 = 'http://sathkara.pensions.gov.lk:8080/pms_revision_forces/api';
    // BASE_PATH1  ='/pms-api/api'; 
    // BASE_PATH1  ='/pms_wnop_v1/api'; 
    FILE_SERVER = "http://192.168.100.198:9010/service";
    SMS_API = "http://sathkara.pensions.gov.lk:8080/sms-api/sms";

    constructor() {
    }

    /**
     * subpaths
     * @author Kasun Gunathilaka
     */

    /**
     * pd3 subpaths
     */
    //authentication
    AUTHENTICATION = this.BASE_PATH + "/employees/authenticate/login";

    //pd3 list view
    PD3LISTVIEW(state: string) {
        return this.BASE_PATH + "/pensioners?state=" + state;
    }

    // view pensioner's pensions
    PD3_VIEW_PENSIONS(refNo: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/pensions";
    }

    //search pd3 pensioner

    PD3_SEARCH_PENSIONER(refNo: number, name: string, nic: string, penNo: number) {
        return this.BASE_PATH + "/pensioners?state=search&refno=" + refNo + "&name=" + name + "&nic=" + nic + "&pensionNumber=" + penNo;
    }

    PD3_SEARCH_PENSIONER_BY_NIC(nic: string) {
        return this.BASE_PATH + "/pensioners?state=search&nic=" + nic;
    }

    PD3_SEARCH_PENSIONER_BY_NAME(name: string) {
        return this.BASE_PATH + "/pensioners?state=search&name=" + name;
    }

    PD3_SEARCH_PENSIONER_BY_PENNO(penNo: number) {
        return this.BASE_PATH + "/pensioners?state=search&name=" + penNo;
    }

    // pension history
    VIEW_PD3_PENSION_HISTORY(refNo: number, pensionNo: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/pensions/" + pensionNo + "/history";
    }

    //get a single pensioner's details
    GET_PENSIONER_DETAILS(refNo: number) {
        return this.BASE_PATH + "/pensioners/" + refNo;
    }

    //receive pd3 pension
    RECEIVE_PD3_PENSIONER(refNo: number, pensionNumber: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/pensions/" + pensionNumber + "/state?verify=true";
    }

    GET_PD3_PENSION_DETAILS(refNumber: number, pensionNumber: number) {
        return this.BASE_PATH + "/pensioners/" + refNumber + "/pensions/" + pensionNumber;
    }

    //pensioner's photo
    GET_PENSIONER_PHOTO(refNo: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/assets/image";
    }

    // retrieve single dependent
    GET_SINGLE_DEPENDENT(refNo: number, dependentId: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/dependents/" + dependentId;
    }

    // retrieve dependent array
    GET_DEPENDENT_ARRAY(refNo: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/dependents/";
    }

    // consolidated salary 
    GET_CONSOLIDATED_SALARY(scale: string, grade: string, salary: number, circular: string, retired_date: string, increment_date: string) {
        return this.BASE_PATH + "/salary?scale=" + scale + "&grade=" + grade + "&salary=" + salary + "&circular=" + circular + "&retired_date="
            + retired_date + "&increment_date=" + increment_date + "&target=2020";
    }

    // gratuity & bank details
    GET_GRATUITY_BANK_DETAILS(refNo: number, gratuityNumber: string) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/gratuities/" + gratuityNumber;
    }

    // get list of gratuities
    GET_ALL_GRATUITIES(refNo: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/gratuities";
    }

    // reject pension application
    REJECT_APPLICATION(refNo: number, pensionNumber: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/pensions/" + pensionNumber + "/state?verify=false";
    }

    // accept pension application
    ACCEPT_APPLICATION(refNo: number, pensionNumber: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/pensions/" + pensionNumber + "/state?verify=true";
    }

    //get all rejection reasons
    GET_ALL_REJECTION_REASONS() {
        return this.BASE_PATH + "/rejections/reasons?type=pd3";
    }

    //save a rejection
    SAVE_REJECTION() {
        return this.BASE_PATH + "/rejections";
    }

    // retrieve all pending gratuities
    GET_PENDING_GRATUITIES(state: number) {
        return this.BASE_PATH + "/pensioners/1/gratuities?state=search&status=" + state;
    }

    //accept gratuity
    ACCEPT_GRATUITIES(pensionerNumber: number, gratuityId: number, state: number) {
        return this.BASE_PATH + "/pensioners/" + pensionerNumber + "/gratuities/" + gratuityId + "/state?changeto=" + state;
    }

    //get branch details
    GET_BRANCH_DETAILS(bankId: string, branchId: string) {
        return this.BASE_PATH + "/banks/" + bankId + "/branches/" + branchId;
    }

    //print letter
    PRINT_LETTER(refNo: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/interview";
    }

    //get pd3 extra-payment details
    EXTRA_PAYMENT_DETAILS(refNo: number, pensionNo: number) {
        return this.BASE_PATH + "/pensioners/" + refNo + "/pensions/" + pensionNo + "/extras";
    }

    GET_INTERVIEW_DETAILS(penNo: number) {
        return this.BASE_PATH + "/pensioners/" + penNo + "/interview";
    }

    SEND_INTERVIEW_SMS() {
        return this.SMS_API;
    }



    /**
     * pd3 report callers
     */

    //pd3 postl report
    POSTAL_REPORT(date: string) {
        return this.BASE_PATH + "/reports/pensions?state=200&date=" + date;
    }

    SAVE_IN_FILE_SERVER(temparam: string, idparam: string) {
        return this.FILE_SERVER + "/upload?temparam=" + temparam + "&idparam=" + idparam;
    }


    //PD6 DETAILS

    // Search Pensioner using NIC

}