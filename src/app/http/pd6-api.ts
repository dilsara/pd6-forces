import { Injectable } from "@angular/core";
import { API } from "./api";
import { yearsPerPage } from "@angular/material/datepicker/typings/multi-year-view";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { NgIfContext } from "@angular/common";
import { pipeDef } from "@angular/core/src/view";

@Injectable({
    providedIn: 'root'
})
export class PDSIXAPI {

    //Basepath
    BASE_PATH = "http://sathkara.pensions.gov.lk:8080/pms_revision_forces/api";
    BASE_PATH_DISTRCIT = "http://sathkara.pensions.gov.lk:8080/pms_masterdata_api/api";
    BASE_PATH_CALCULATION = "http://sathkara.pensions.gov.lk:8080/calculationsapi";
    BASE_PATH_SMS = "http://sathkara.pensions.gov.lk:8080/sms-api/sms";
    // BASE_GAMMA = "http://192.168.100.30:8080/calculations_api_v2/salary"
    BASE_GAMMA = "http://203.115.24.107:9090/calculations_api_v2/salary"

    constructor(
        private baseapi: API
    ) {
    }

    //Search PD6 Pensioner using NIC
    PD6_SEARCH_PENSIONER_BY_NIC(nic: string) {
        return this.BASE_PATH + "/pension/" + nic;
    }

    //Save details to DB
    SAVE_PD6_DETAILS() {
        return this.baseapi.BASE_PATH1 + "/pd6-revision";
    }

    //Retreieve District details using dscode
    GET_DISTRCIT_DETAILS(dscode: number) {
        return this.BASE_PATH + "/ds/dscode/" + dscode;
    }

    //REVISON CALCULATION
    GET_CALCULATION(scale: String, grade: String, salary: number, circular: String, retired_date: String, increment_date: String, target: String) {
        return this.BASE_PATH_CALCULATION + "/salary?scale=" + scale + "&grade=" + grade + "&salary=" + salary
            + "&circular=" + circular + "&retired_date=" + retired_date + "&increment_date=" + increment_date + "&target=" + target
    }

    //Get percentage details for reduced and unreduced calculation - CIVIL
    GET_PERCENTAGE(type: String, month: number, salary: number, circular: String, years: number) {
        return this.BASE_PATH_CALCULATION + "/percentages?type=" + type + "&months=" + month +
            "&salary=" + salary + "&circular=" + circular + "&years=" + years;
    }

    //Get percentage details for reduced and unreduced calculation - MILITORY
    GET_PERCENTAGE_MILITORY(type: String, month: number, salary: number, circular: String, years: number, offeredto: String) {
        return this.BASE_PATH_CALCULATION + "/percentages?type=" + type + "&months=" + month +
            "&salary=" + salary + "&circular=" + circular + "&years=" + years + "&offeredto=" + offeredto;
    }

    //Send SMS after submission
    SEND_SMS() {
        return this.BASE_PATH_SMS;
    }

    //POST dashboard counts
    DASHBOARD_COUNTS() {
        return this.baseapi.BASE_PATH1 + "/profile/dashbord";
    }

    //Retrieve submitted pensioner details
    GET_PD6PENSIONER_DETAILS(ref: number) {
        return this.baseapi.BASE_PATH1 + "/pd6-revision/" + ref;
    }

    //Retrieve NIC Checks
    GET_NIC_DETAILS(nic: String) {
        return this.baseapi.BASE_PATH1 + "/profile/nic/" + nic;
    }

    //Dashboard list view
    LIST_VIEW(username: String, state: number) {
        return this.baseapi.BASE_PATH1 + "/pd6-revision/" + username + "/" + state;
    }

    //PUT values after editing
    EDIT_PENSIONER_DETAILS(ref: number) {
        return this.baseapi.BASE_PATH1 + "/pd6-revision/" + ref;
    }

    //State change
    CHANGE_STATE() {
        return this.baseapi.BASE_PATH1 + "/pd6-revision/status";
    }

    //Check DScode
    CHECK_DSCODE(username: String, nic: String) {
        return this.BASE_PATH + "/pd6-revision/check-ds/" + username + "/" + nic;
    }

    //GET DESIGNATION LIST
    GET_DESIGNATIONLIST() {
        return this.BASE_PATH + "/designations";
    }

    //GET SERVICE LIST
    GET_SERVICELIST() {
        return this.BASE_PATH + "/services"
    }

    //CHECK WHETHE RTHIS NIC CONSISTS ONE OR TWO PENSIONS
    CHECK_NIC_AVAILABILTY(nic: String) {
        return this.BASE_PATH + "/pd6-revision/availability/" + nic;
    }

    //GET PERSONAL INFO FOR ALREADY EXISTS NIC
    GET_PEROSNALINFO(nic: String, pt: number) {
        return this.BASE_PATH + "/pd6-revision/person/" + nic + "/" + pt;
    }

    //Retrieve EDward sir's information for source document adn Edit using PID
    GET_PENSIONER_BYPID(pid: number, status: string) {
        return this.BASE_PATH + "/pension/bypid/" + pid + "/" + status;
    }

    //Get Step
    GET_STEP(scale: String, grade: String, salary: number, retired_date: String,
        increment_date: String) {
        return this.BASE_GAMMA + "/v2?scale=" + scale + "&grade=" +
            grade + "&salary=" + salary + "&circular=2015" + "&retired_date=" +
            retired_date + "&increment_date=" + increment_date + "&target=2015";
    }

    //GET NEW SCALE,GRADE AND SALARY
    GET_NEW_DETAILS(scale: String, grade: String, step: number) {
        return this.BASE_GAMMA + "/convert?scale=" + scale + "&grade=" + grade + "&circular=2017&step=" + step;
    }

    //Get from 192.168.100.145 - Pension (Data migrated)
    GET_JUNE_RECORDS(pid: number, status: number) {
        return this.BASE_PATH + "/pension/june/" + pid + "/" + status;
    }

    //Get DISTRICT OFFICER - Dashboard counts
    GET_DASHBOARD_COUNTS_DO() {
        return this.BASE_PATH + "/profile/bydistrict/dashbord";
    }

    //Get DISTRICT OFFICER - Dashboard List View
    GET_DASHBOARD_LISTVIEW_DO(state: number) {
        return this.BASE_PATH + "/pd6-revision/bydistrict/" + state;
    }

    //POST 48% WITH REASONS
    POST_REVISION_ABOVE48() {
        return this.BASE_PATH + "/pd6-revision/revisionAbove48";
    }

    UPDATE_REVISIONREASON_ABOVE_48(refNumber: number) {
        return this.BASE_PATH + "/pd6-revision/revisionAbove48/" + refNumber;
    }

    RE_REVISION_REASON() {
        return this.BASE_PATH + "/pd6-revision/re-revisionReason";
    }
}