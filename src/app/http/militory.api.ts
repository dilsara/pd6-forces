import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class MilitoryAPI {

    // bASE PATH
    BASE_PATH = 'http://192.168.102.191:8080/pms-api/api';

    /**
     * 
     * @author Nuwan Dilsara
     */

    //authentication
    AUTHENTICATION = this.BASE_PATH + "/employees/authenticate/login";

    /**
     * get district list
     */
    DISTRICT_LIST() {
        return this.BASE_PATH + '/districts';
    }

    /**
     * get district secretariat list
     * @param district_id
     */
    DS_LIST(district_id: Number){
        return this.BASE_PATH + '/ds/' + district_id;
    }

    /**
     * GET GRAMA NILADARI DIVISION
     * @param ds_id 
     */
    GN_DIVISION_LIST(ds_id: Number){
        return this.BASE_PATH + '/gn/' + ds_id;
    }

    /**
     * GET DESIGNATION
     */
    DESIGNATIONS() {
        return this.BASE_PATH + '/designations';
    }

    /**
     * GET GRADES
     */
    GRADES() {
        return this.BASE_PATH + '/grades';
    }

    /**
     * GET SALARY CODE
     */
    SALARY_CODES() {
        return this.BASE_PATH + '/salarycodes';
    }

    /**
     * GET SERVICES
     */
    SERVICES() {
        return this.BASE_PATH + '/services';
    }

}