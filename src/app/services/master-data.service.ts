import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API } from '../http/api';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';
import { DistrictsModel } from '../layout/wnop/model/districtsModel.model';
import { DSModel } from '../layout/wnop/model/DSModel.model';
import { GNModel } from '../layout/wnop/model/GNModel.model';
import { IdNameModel } from '../models/id-name.model';
import { NicList } from '../layout/wnop/model/nicList.model';

@Injectable({
  providedIn: 'root'
})
export class MasterDataService {

  constructor(
    private http: HttpClient,
    private api: API,
    private configService: ConfigService,
  ) { }

 
  //get pensioner's photo
  GET_PENSIONER_PHOTO(refNo: String) {
    return this.http.get(this.api.FILE_SERVER + "/download?temparam=13&idparam="+refNo);
}

 

  SAVE_IN_FILE_SERVER(temparam:string,idparam:string,payload: any) {
    return this.http.post(this.api.FILE_SERVER + "/upload?temparam=" + temparam + "&idparam=" + idparam,payload);
  }

  // GET District list fron api
  getDistrictList(): Observable<DistrictsModel[]> {
    // return this.http.get<DistrictsModel[]>(this.api.MASTER_DATA_BASE_PATH + '/supportive/districts');
    return this.http.get<DistrictsModel[]>(this.api.BASE_PATH1 + '/districts');
  }

  // GET District by id fron api
  getDistricts(districtId:number): Observable<DistrictsModel> {
    // return this.http.get<DistrictsModel[]>(this.api.MASTER_DATA_BASE_PATH + '/supportive/districts');
    return this.http.get<DistrictsModel>(this.api.BASE_PATH1 + '/districts/' + districtId);
  }

  // GET DS list fron api by district id
  getDsList(districtId): Observable<DSModel[]> {
    // return this.http.get<DSModel[]>(this.api.MASTER_DATA_BASE_PATH + '/supportive/districts/' + districtId + '/dsoffice');
    return this.http.get<DSModel[]>(this.api.BASE_PATH1 + '/ds/' + districtId );
  }

  // GET District fron api
  getDs(dsId:number): Observable<DSModel> { 
    return this.http.get<DSModel>(this.api.BASE_PATH1 + '/ds/' + dsId +'/get');
  }

  // GET GN list fron api
  getGnList(dsId): Observable<GNModel[]> {
    // return this.http.get<GNModel[]>(this.api.MASTER_DATA_BASE_PATH + '/supportive/districts/' + districtId + '/dsoffice/' + dsId + '/vo');
    return this.http.get<DSModel[]>(this.api.BASE_PATH1 + '/gn/' + dsId );
  }

  // GET GN  fron api
  getGn(gnId:number): Observable<DSModel> { 
    return this.http.get<DSModel>(this.api.BASE_PATH1 + '/gn/' + gnId +'/get');
  }

  // GET Designations list fron api
  getDesignationsList(): Observable<IdNameModel[]> {
    return this.http.get<IdNameModel[]>(this.api.BASE_PATH1 + '/designations');
  }

  // GET Designation fron api
  getDesignations(Id:number): Observable<IdNameModel> { 
    return this.http.get<IdNameModel>(this.api.BASE_PATH1 + '/designations/' + Id);
  }

  //GET Salary Scale list fron api
  getSalaryScaleList(): Observable<String[]> {
    return this.http.get<String[]>(this.api.BASE_PATH1 + '/salarycodes');
  }

  // GET Salary Scale fron api
  getSalaryScale(Id:number): Observable<IdNameModel> { 
    return this.http.get<IdNameModel>(this.api.BASE_PATH1 + '/salarycodes/' + Id);
  }

  // GET Services list fron api
  getServiceList(): Observable<String[]> {
    return this.http.get<String[]>(this.api.BASE_PATH1 + '/services');
  }

  // GET Service Scale fron api
  getService(Id:number): Observable<IdNameModel> { 
    return this.http.get<IdNameModel>(this.api.BASE_PATH1 + '/services/' + Id);
  }

  // GET Institute list fron api 
  getInstituteList() {    
    return this.http.get<IdNameModel[]>(this.api.BASE_PATH1 + '/pensionpoints');
  }

  // GET Institute fron api
  getInstitute(Id:number): Observable<IdNameModel> { 
    return this.http.get<IdNameModel>(this.api.BASE_PATH1 + '/pensionpoints/' + Id);
  }

  // GET Services list fron api
  getGradeList(): Observable<String[]> {
    return this.http.get<String[]>(this.api.BASE_PATH1 + '/grades');
  }

  // GET Grade fron api
  getGrade(Id:number): Observable<IdNameModel> { 
    return this.http.get<IdNameModel>(this.api.BASE_PATH1 + '/grades/' + Id);
  }

  // GET District fron api
  checkNIC(nic:string): Observable<NicList> { 
    return this.http.get<NicList>(this.api.BASE_PATH1 + '/profile/nic/' + nic );
  }
}
