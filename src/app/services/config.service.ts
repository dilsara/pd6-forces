import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Authentication } from '../models/auth.model';

@Injectable({
	providedIn: 'root'
})

export class ConfigService {

	constructor(
		private http: HttpClient
	) { }

	/**
	 * service callers 
	 */

	//http get
	get(url: string) {
		return this.http.get(url);
	}

	//http get with session id
	getWithToken(url: string) {
		const httpOptions = {
			headers: new HttpHeaders({
				'token': localStorage.getItem("token")
				// 'token': "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzMiIsImlhdCI6MTU2ODg4NjkyOCwic3ViIjoiYXV0aF90b2tlbiIsImlzcyI6InBlbnNpb25kcHQiLCJiIjoidGVzdHBvMyIsImEiOiJEU19QRU5TSU9OX09GRklDRVIiLCJkIjoxLCJleHAiOjE1Njg5MTU3Mjh9.ENChiqpC5RHzTacKsQGVifZ82Mfw7DgOmFEhVlCTpbs"

			})
		};
		return this.http.get(url, httpOptions);
	}

	//http post
	post(url: string, data: any) {
		return this.http.post(url, data);
	}

	//http post with session id
	postWithToken(url: string, data: any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'token': localStorage.getItem("token")
				// 'token': "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzMiIsImlhdCI6MTU2ODg4NjkyOCwic3ViIjoiYXV0aF90b2tlbiIsImlzcyI6InBlbnNpb25kcHQiLCJiIjoidGVzdHBvMyIsImEiOiJEU19QRU5TSU9OX09GRklDRVIiLCJkIjoxLCJleHAiOjE1Njg5MTU3Mjh9.ENChiqpC5RHzTacKsQGVifZ82Mfw7DgOmFEhVlCTpbs"
			})
		};
		return this.http.post(url, data, httpOptions);
	}

	//http put
	put(url: string, data: any) {
		const httpOptions = {
			headers: new HttpHeaders({
				'token': localStorage.getItem("token")
				// 'token': "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzMiIsImlhdCI6MTU2ODg4NjkyOCwic3ViIjoiYXV0aF90b2tlbiIsImlzcyI6InBlbnNpb25kcHQiLCJiIjoidGVzdHBvMyIsImEiOiJEU19QRU5TSU9OX09GRklDRVIiLCJkIjoxLCJleHAiOjE1Njg5MTU3Mjh9.ENChiqpC5RHzTacKsQGVifZ82Mfw7DgOmFEhVlCTpbs"
			})
		};
		return this.http.put(url, data, httpOptions);
	}

	//http delete
	delete(url: string) {
		return this.http.delete(url);
	}


}
