import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  listViewSpinner: boolean;
  viewPensionsSpinner: boolean;

  constructor() {
    this.listViewSpinner = false;
    this.viewPensionsSpinner = false;
  }
}
