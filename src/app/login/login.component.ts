import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../router.animations';
import { AuthenticateService } from '../services/authenticate.service';
import { NotificationsService } from '../services/notifications.service';



@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    constructor(
        private translate: TranslateService,
        public router: Router,
        public auth: AuthenticateService,
        private notify: NotificationsService
    ) {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
    }

    ngOnInit() { }

    // onLoggedin() {

    // }

    authenticate() {
        this.auth.authenticate()
            .subscribe(data => {
                localStorage.setItem('isLoggedin', 'true');
                localStorage.setItem('username', data["username"])
                localStorage.setItem('userRole', data["role"]);
                localStorage.setItem('sessionId', data["sessionId"]);
                localStorage.setItem('userId', '1');
                this.router.navigateByUrl("/dashboard");
            }, error => {
                this.notify.openSnackBar("Login Failed", "Check username & password", 'red');
            }).closed;
    }

    keyDownFunction(event) {
        if(event.keyCode == 13) {
            this.auth.authenticate()
            .subscribe(data => {
                localStorage.setItem('isLoggedin', 'true');
                localStorage.setItem('username', data["username"])
                localStorage.setItem('userRole', data["role"]);
                localStorage.setItem('sessionId', data["sessionId"]);
                localStorage.setItem('userId', '1');
                this.router.navigateByUrl("/dashboard");
            }, error => {
                this.notify.openSnackBar("Login Failed", "Check username & password", 'red');
            }).closed;
        }
      }
 
}
