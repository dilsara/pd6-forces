export class Pd3SideBarConfig {

    pd3SidebarConfig: object[];

    constructor() {

    }

    getSideBarConfig(userRole: string) {
        if (userRole == "POSTAL") {
            this.pd3SidebarConfig = [
                {
                    "menuItem": "Pensioner Details",
                    "path": "/pd3-pensions"
                },
                {
                    "menuItem": "Reports",
                    "path": "/pd3-pensions/reports"
                },
                {
                    "menuItem": "Quick Access",
                    "path": "/pd3-pensions/quick-access"
                }
            ]
        }
        if (userRole == "DATA_ENTRY_OFFICER") {
            this.pd3SidebarConfig = [
                {
                    "menuItem": "Pensioner Details",
                    "path": "/pd3-pensions"
                },
                {
                    "menuItem": "Reports",
                    "path": "/pd3-pensions/reports"
                }
            ]
        }

        if (userRole == "PRINT_LETTER") {
            this.pd3SidebarConfig = [
                {
                    "menuItem": "Print Letter",
                    "path": "/pd3-pensions"
                }
            ]
        }

        if (userRole == "SATHKARA_OFFICER") {
            this.pd3SidebarConfig = [
                {
                    "menuItem": "Pensioner Details",
                    "path": "/pd3-pensions"
                },
                {
                    "menuItem": "Reports",
                    "path": "/pd3-pensions/reports"
                }
            ]
        }

        if (userRole == "REGISTRATION_AD") {
            this.pd3SidebarConfig = [
                {
                    "menuItem": "Gratuity Details",
                    "path": "/pd3-pensions/pd3-view-gratuities"
                }
            ]
        }

        if (userRole == "ACCOUNT_CLERK") {
            this.pd3SidebarConfig = [
                {
                    "menuItem": "Gratuity Details",
                    "path": "/pd3-pensions/pd3-view-gratuities"
                }
            ]
        }
    }
}