export class wnopSideBarConfig {

    wnopSideBarConfig: object[];

    constructor() {

    }

    getSideBarConfig(userRole: string) {        

        if (userRole == "PENSION_POINT") {
            this.wnopSideBarConfig = [
                {
                    "menuItem": "Dashbord",
                    "path": "/wnop"
                },
                {
                    "menuItem": "New Application",
                    "path": "/wnop/reregistration/true"
                },
                {
                    "menuItem": "Re-Registration",
                    "path": "/wnop/reregistration/false"
                },
                {
                    "menuItem": "Update Application",
                    "path": "/wnop/update/0"
                },
                {
                    "menuItem": "Retrive Application",
                    "path": "/wnop/print/0"
                },
                {
                    "menuItem": "Print W&OP Card",
                    "path": "/wnop"
                }
            ]
        }

        else {
            this.wnopSideBarConfig = [
                {
                    "menuItem": "Gratuity Details",
                    "path": "/wnop"
                }
            ]
        }
    }
}