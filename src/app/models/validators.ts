import { AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';
import { of } from 'rxjs/internal/observable/of';
// import { of } from 'rxjs/observable/of';
import { map } from 'rxjs/internal/operators/map'; 

export function ValidateForName(control: AbstractControl) {
    const expression: RegExp = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
  
    return of(expression.test(control.value)).pipe(
      map(result => (!result ? { invalid: true } : null)
      )
    );
  }

  export function ValidatenumberWithChar(): ValidatorFn {  
    return (control: AbstractControl) => {
      const expression: RegExp = /^[a-z,A-Z,0-9]*$/;
      const Validatenumber: String = control.value;
  
      if ((Validatenumber.length == 0)) {
        return  null;
      } else {
        return !expression.test(control.value) ? {'numberWithName': true} : null;
      }    
    };
  }

  export function ValidateForNic(control: AbstractControl) {
    const expression: RegExp = /[0-9]{9}[X|V]$/;
    const newExpression: RegExp = /^[1-9]{1}[0-9]{11,11}$/;
    //const newExpression: RegExp = /^[1-9]{12,12}$/;
  
    const nic: String = control.value;
    if ((nic.length <= 10)) {
      return of(expression.test(control.value)).pipe(
        map(result => (!result ? { 'invalid': true } : null)
        )
      );
  
    } else {
      return of(newExpression.test(control.value)).pipe(
        map(result => (!result ? { 'invalid': true } : null)
        )
      );
    }
  }
  
  export function ValidateForNumber(control: AbstractControl) {
    const expression: RegExp = /^[0-9]*$/;
  
    return of(expression.test(control.value)).pipe(
      map(result => (!result ? { invalid: true } : null)
      )
    );
  }

  export function NameValidator(): ValidatorFn {
    return (control: AbstractControl) => {
      const expression: RegExp = /^([a-zA-Z][ ]?)*$/;
      return !expression.test(control.value) ? {'name': true} : null;
    };
  }

  export function ValidateForPhone(): ValidatorFn {
    return (control: AbstractControl) => {
      const expression: RegExp = /^[0-9]{10,10}$/;
      const phone: String = control.value;
  
      if ((phone.length == 0)) {
        return null;
      }else {
        return !expression.test(control.value) ? {'phone': true} : null;
      }    
    };
  }

  export function emailValidator(): ValidatorFn {
    return (control: AbstractControl) => {
      const expression: RegExp = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      const Validatenumber: String = control.value;
  
      if ((Validatenumber.length == 0)) {
        return  null;
      } else {
        return !expression.test(control.value) ? {'email': true} : null;
      }
    };
  }