import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DataConfigService } from '../services/data-config.service';
import { Pd3PensionsService } from '../services/pd3-pensions.service';
import { Pd3GratuityService } from '../services/pd3-gratuity.service';
import { PD3ButtonConfigModel } from 'src/app/models/pd3-button-config.model';
import { Calculations } from '../models/calculations.model';

@Component({
  selector: 'app-pd3-detailed-view',
  templateUrl: './pd3-detailed-view.component.html',
  styleUrls: ['./pd3-detailed-view.component.scss']
})
export class Pd3DetailedViewComponent implements OnInit {

  refNumber: number = 0; //initialize pensioner number
  pensionNumber: number = 0;//initialize pension number
  state: string;
  buttonConfig: PD3ButtonConfigModel;

  constructor(
    private activatedRoute: ActivatedRoute,
    public dataConfig: DataConfigService,
    private pensionService: Pd3PensionsService,
    private gratuityService: Pd3GratuityService
  ) {
    this.buttonConfig = new PD3ButtonConfigModel();
    this.dataConfig.pd3Calculations = new Calculations();
  }

  ngOnInit() {
    // set pension number from params
    this.activatedRoute.params.subscribe((params: Params) => {
      this.refNumber = params['refNumber'];
      this.pensionNumber = params['pensionNumber']
      this.buttonConfig.getButtonConfiguration(localStorage.getItem("userRole"), this.state);
      this.pensionService.getPd3PensionersDetails(this.refNumber) // pensioner basic information
        .subscribe(data => {

          this.dataConfig.pd3BasicInformation = JSON.parse(JSON.stringify(data));
          this.pensionService.getPd3PensionDetails(this.refNumber, this.pensionNumber) // pension information
            .subscribe(data => {

              this.dataConfig.pd3Pension = JSON.parse(JSON.stringify(data));
              this.pensionService.getConsolidateSalary2020(this.dataConfig.pd3Pension.scale,
                this.dataConfig.pd3BasicInformation.grade, this.dataConfig.pd3Pension.consolidatedSalary / 12,
                this.dataConfig.pd3Pension.circular, this.dataConfig.pd3BasicInformation.dateOfRetired,
                this.dataConfig.pd3BasicInformation.nextIncrementDate) // consolidated salary 2020
                .subscribe(data => {

                  this.dataConfig.pd3ConsolidatedSalary2020 = JSON.parse(JSON.stringify(data));
                  this.gratuityService.getGratuityBankDetails(this.refNumber, this.dataConfig.pd3BasicInformation.gratuity.url)
                    .subscribe(data => {

                      this.dataConfig.pd3GratuityBankDetails = JSON.parse(JSON.stringify(data));
                      this.pensionService.getExtraPaymentDetails(this.refNumber, this.pensionNumber)
                        .subscribe(data => {
                          this.dataConfig.pd3ExtraPayments = data["extraPayments"];
                        })

                      this.dataConfig.pd3ExtraPayments.forEach(element => {
                        if (element.type == "allowance") {
                          this.dataConfig.pd3Calculations.totalAllowance += element.amount;
                        } else {
                          this.dataConfig.pd3Calculations.totalAllowance = 0.00;
                        }
                      })
                      this.calculateSalaries();
                    })
                })

            })
        })
    });

  }

  calculateSalaries() {
    console.log(this.dataConfig.pd3Pension.circular);
    if (this.dataConfig.pd3Pension.circular == '2015') {
      console.log("11111111111111111")
      this.dataConfig.pd3ConsolidatedSalary2020.secondAllowance = 0.00;
      this.dataConfig.pd3ConsolidatedSalary2020.firstAllowance = 0.00;
      this.dataConfig.pd3ConsolidatedSalary2020.basicSalary = 0.00;
      this.dataConfig.pd3Calculations.total = this.dataConfig.pd3Pension.consolidatedSalary + this.dataConfig.pd3ConsolidatedSalary2020.firstAllowance + this.dataConfig.pd3Calculations.totalAllowance;
      this.dataConfig.pd3Calculations.total2020 = (this.dataConfig.pd3ConsolidatedSalary2020.basicSalary * 12.0) + this.dataConfig.pd3ConsolidatedSalary2020.secondAllowance + this.dataConfig.pd3Calculations.totalAllowance;
      this.dataConfig.pd3Calculations.reducedSalary = (this.dataConfig.pd3Calculations.total * this.dataConfig.pd3Pension.netReducedPercentage / 100.0) / 12.0;
      this.dataConfig.pd3Calculations.unreducedSalary = (this.dataConfig.pd3Calculations.total * this.dataConfig.pd3Pension.netUnreducedPercentage / 100.0) / 12.0;
      this.dataConfig.pd3Calculations.reducedSalary2020 = (this.dataConfig.pd3Calculations.total2020 * (this.dataConfig.pd3Pension.netReducedPercentage / 100.0)) / 12.0;
      this.dataConfig.pd3Calculations.unreducedSalary2020 = (this.dataConfig.pd3Calculations.total2020 * (this.dataConfig.pd3Pension.netUnreducedPercentage / 100.0)) / 12.0;
    } else {
      console.log("222222222222222")
      this.dataConfig.pd3Calculations.total = this.dataConfig.pd3Pension.consolidatedSalary + this.dataConfig.pd3ConsolidatedSalary2020.firstAllowance + this.dataConfig.pd3Calculations.totalAllowance;
      this.dataConfig.pd3Calculations.total2020 = (this.dataConfig.pd3ConsolidatedSalary2020.basicSalary * 12.0) + this.dataConfig.pd3ConsolidatedSalary2020.secondAllowance + this.dataConfig.pd3Calculations.totalAllowance;
      this.dataConfig.pd3Calculations.reducedSalary = (this.dataConfig.pd3Calculations.total * this.dataConfig.pd3Pension.netReducedPercentage / 100.0) / 12.0;
      this.dataConfig.pd3Calculations.unreducedSalary = (this.dataConfig.pd3Calculations.total * this.dataConfig.pd3Pension.netUnreducedPercentage / 100.0) / 12.0;
      this.dataConfig.pd3Calculations.reducedSalary2020 = (this.dataConfig.pd3Calculations.total2020 * (this.dataConfig.pd3Pension.netReducedPercentage / 100.0)) / 12.0;
      this.dataConfig.pd3Calculations.unreducedSalary2020 = (this.dataConfig.pd3Calculations.total2020 * (this.dataConfig.pd3Pension.netUnreducedPercentage / 100.0)) / 12.0;
    }

  }


}
