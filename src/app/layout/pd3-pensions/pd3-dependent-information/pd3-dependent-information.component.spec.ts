import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd3DependentInformationComponent } from './pd3-dependent-information.component';

describe('Pd3DependentInformationComponent', () => {
  let component: Pd3DependentInformationComponent;
  let fixture: ComponentFixture<Pd3DependentInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd3DependentInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd3DependentInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
