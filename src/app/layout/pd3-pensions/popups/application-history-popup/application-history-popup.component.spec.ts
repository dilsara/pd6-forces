import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationHistoryPopupComponent } from './application-history-popup.component';

describe('ApplicationHistoryPopupComponent', () => {
  let component: ApplicationHistoryPopupComponent;
  let fixture: ComponentFixture<ApplicationHistoryPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationHistoryPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationHistoryPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
