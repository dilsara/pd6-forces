import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveApplicationPopupComponent } from './receive-application-popup.component';

describe('ReceiveApplicationPopupComponent', () => {
  let component: ReceiveApplicationPopupComponent;
  let fixture: ComponentFixture<ReceiveApplicationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiveApplicationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveApplicationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
