import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataConfigService } from '../../services/data-config.service';
import { Pd3GratuityService } from '../../services/pd3-gratuity.service';

@Component({
  selector: 'app-gratuity-bank-details-popup',
  templateUrl: './gratuity-bank-details-popup.component.html',
  styleUrls: ['./gratuity-bank-details-popup.component.scss']
})
export class GratuityBankDetailsPopupComponent implements OnInit {

  gratuityBank: any;
  pensionBank: any;

  constructor(
    private dialogRef: MatDialogRef<GratuityBankDetailsPopupComponent>,
    public dataConfig: DataConfigService,
    public gratuityService: Pd3GratuityService,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
    this.gratuityService.getGratuityBank(this.data.gratuitybranch)
      .subscribe(data => {
        this.gratuityBank = data;
      })
    this.gratuityService.getGratuityBank(this.data.pensionBranch)
      .subscribe(data => {
        this.pensionBank = data;
      })
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

}
