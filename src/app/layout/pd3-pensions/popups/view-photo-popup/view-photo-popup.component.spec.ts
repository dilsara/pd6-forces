import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPhotoPopupComponent } from './view-photo-popup.component';

describe('ViewPhotoPopupComponent', () => {
  let component: ViewPhotoPopupComponent;
  let fixture: ComponentFixture<ViewPhotoPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPhotoPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPhotoPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
