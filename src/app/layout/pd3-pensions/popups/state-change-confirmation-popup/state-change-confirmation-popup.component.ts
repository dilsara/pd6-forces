import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Pd3PensionsService } from '../../services/pd3-pensions.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-state-change-confirmation-popup',
  templateUrl: './state-change-confirmation-popup.component.html',
  styleUrls: ['./state-change-confirmation-popup.component.scss']
})
export class StateChangeConfirmationPopupComponent implements OnInit {

  refNumber: number;
  pensionNumber: number;
  state: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<StateChangeConfirmationPopupComponent>,
    private pensionsService: Pd3PensionsService,
    private router: Router,
    private notify: NotificationsService
  ) { }

  ngOnInit() {

  }

  stateChange() {
    if (this.data.state == "reject") {
      this.pensionsService.rejectApplication(this.data.referenceNumber, this.data.pensionNo)
        .subscribe(() => {
          this.notify.openSnackBar("Pension application rejected successfully", "", 'green');
          this.router.navigateByUrl("/pd3-pensions/pd3-list-view/" + this.data.applicationState);
        })
    }

    if (this.data.state == "accept") {
      this.pensionsService.acceptApplication(this.data.referenceNumber, this.data.pensionNo)
        .subscribe(() => {
          this.notify.openSnackBar("Pension application accepted successfully", "", 'green');
          this.router.navigateByUrl("/pd3-pensions/pd3-list-view/" + this.data.applicationState);
        })
    }
  }

  close(): void {
    this.dialogRef.close();
  }

}
