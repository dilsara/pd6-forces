import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptGratuitiesPopupComponent } from './accept-gratuities-popup.component';

describe('AcceptGratuitiesPopupComponent', () => {
  let component: AcceptGratuitiesPopupComponent;
  let fixture: ComponentFixture<AcceptGratuitiesPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptGratuitiesPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptGratuitiesPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
