import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DataConfigService } from '../../services/data-config.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-extra-payment-details-popup',
  templateUrl: './extra-payment-details-popup.component.html',
  styleUrls: ['./extra-payment-details-popup.component.scss']
})
export class ExtraPaymentDetailsPopupComponent implements OnInit {

  allowances = [];
  deductions = [];

  constructor(
    private dialogRef: MatDialogRef<ExtraPaymentDetailsPopupComponent>,
    public dataConfig: DataConfigService
  ) { }

  ngOnInit() {
    this.getExtraPayment();
  }

  close(): void {
    this.dialogRef.close(); //close dialog
  }

  getExtraPayment() {
    this.dataConfig.pd3ExtraPayments.forEach(element => {
      if (element.type == "deduction") {
        this.deductions.push(element)
      } else {
        this.allowances.push(element);
      }
    })
  }

}
