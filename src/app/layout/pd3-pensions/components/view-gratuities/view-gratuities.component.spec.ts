import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewGratuitiesComponent } from './view-gratuities.component';

describe('ViewGratuitiesComponent', () => {
  let component: ViewGratuitiesComponent;
  let fixture: ComponentFixture<ViewGratuitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGratuitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewGratuitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
