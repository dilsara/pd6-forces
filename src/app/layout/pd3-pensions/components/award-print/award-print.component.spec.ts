import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwardPrintComponent } from './award-print.component';

describe('AwardPrintComponent', () => {
  let component: AwardPrintComponent;
  let fixture: ComponentFixture<AwardPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwardPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwardPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
