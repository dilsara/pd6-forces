import { Component, OnInit, ViewChildren } from '@angular/core';
import { ReceiveApplicationPopupComponent } from '../../popups/receive-application-popup/receive-application-popup.component';
import { MatDialog } from '@angular/material';
import { Pd3PensionsService } from '../../services/pd3-pensions.service';
import { PD3PensionerModel } from '../../models/pensioner.model';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-quick-access',
  templateUrl: './quick-access.component.html',
  styleUrls: ['./quick-access.component.scss']
})
export class QuickAccessComponent implements OnInit {

  @ViewChildren('input') vc;
  refNumber: number;
  pensions: string[];
  pensioner: PD3PensionerModel;
  constructor(
    private dialog: MatDialog,
    private pensionService: Pd3PensionsService
  ) {
    this.pensioner = new PD3PensionerModel();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {            
    this.vc.first.nativeElement.focus();
}

  // popup for gratuity details
  receiveApplicationPopup(): void {
    console.log(this.pensioner)
    const dialogRef = this.dialog.open(ReceiveApplicationPopupComponent, {
      width: '650px',
      height: '450px',
      data: { pensions: this.pensions, pensioner: this.pensioner }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refNumber = null;
    });
  }

  onEnter(event) {
    if (event.keyCode == 13) {
      this.pensionService.getPd3PensionersDetails(this.refNumber)
        .subscribe(data => {
          this.pensioner = JSON.parse(JSON.stringify(data));
          this.pensionService.getPensions(this.refNumber)
            .subscribe(data => {
              this.pensions = data["pensions"];
              this.receiveApplicationPopup();
            }, error => {

            })
        }, error => {

        })

    }
  }
}
