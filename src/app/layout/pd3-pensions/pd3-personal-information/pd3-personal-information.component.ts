import { Component, OnInit, Inject } from '@angular/core';
import { DataConfigService } from '../services/data-config.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ContactDetailsPopupComponent } from '../popups/contact-details-popup/contact-details-popup.component';
import { ViewPhotoPopupComponent } from '../popups/view-photo-popup/view-photo-popup.component';
import { ActivatedRoute, Params } from '@angular/router';
import { Pd3PensionsService } from '../services/pd3-pensions.service';

@Component({
  selector: 'app-pd3-personal-information',
  templateUrl: './pd3-personal-information.component.html',
  styleUrls: ['./pd3-personal-information.component.scss']
})
export class Pd3PersonalInformationComponent implements OnInit {

  referenceNumber: number;
  pensionerImage: string;

  constructor(
    public dataConfig: DataConfigService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    // @Inject(MAT_DIALOG_DATA) public data: any,
    private pensionService: Pd3PensionsService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.referenceNumber = params['refNumber'];
      this.pensionService.getPensionerPhoto(this.referenceNumber)
        .subscribe(data => {
          this.pensionerImage = "data:image/png;base64," + data["value"];
          // this.convertImage();
        })

    })
  }

  //pop up for contact details
  contactDetailsPopup(): void {
    const dialogRef = this.dialog.open(ContactDetailsPopupComponent, {
      width: '500px',
      height: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  //pop up for contact details
  viewPhotoPopup(): void {
    const dialogRef = this.dialog.open(ViewPhotoPopupComponent, {
      width: '500px',
      height: '350px',
      data: { referenceNumber: this.referenceNumber }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
