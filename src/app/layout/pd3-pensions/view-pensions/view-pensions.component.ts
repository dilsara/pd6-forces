import { Component, OnInit } from '@angular/core';
import { Pd3ViewPensionsService } from '../services/pd3-view-pensions.service';
import { DataConfigService } from '../services/data-config.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import { ApplicationHistoryPopupComponent } from '../popups/application-history-popup/application-history-popup.component';
import { MatDialog } from '@angular/material';
import { Pd3PensionsService } from '../services/pd3-pensions.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { PD3ButtonConfigModel } from 'src/app/models/pd3-button-config.model';

export interface ReferenceNumber {
  refNo: number;
}

@Component({
  selector: 'app-view-pensions',
  templateUrl: './view-pensions.component.html',
  styleUrls: ['./view-pensions.component.scss']
})

export class ViewPensionsComponent implements OnInit {

  public buttonConfig: PD3ButtonConfigModel;
  private state: string;
  private refNumber: number;

  constructor(
    public dataConfig: DataConfigService,
    public spinner: SpinnerService,
    private activatedRoutes: ActivatedRoute,
    private dialog: MatDialog,
    private pensionService: Pd3PensionsService,
    private notify: NotificationsService,
    private router: Router
  ) {
    this.buttonConfig = new PD3ButtonConfigModel();
  }

  ngOnInit() {
    this.activatedRoutes.params.subscribe((params: Params) => {
      this.state = params['state']; // get route parameters
      this.refNumber = params['refNumber']; // get ref number from params
      this.buttonConfig.getButtonConfiguration(localStorage.getItem("userRole"), this.state);
    })
  }

  //popup view for view pension history
  applicationHistoryPopup(refNumber: number): void {
    const dialogRef = this.dialog.open(ApplicationHistoryPopupComponent, {
      width: '950px',
      height: '550px',
      data: { refNo: refNumber }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  //receive  a single pension
  receivePension(pensionNumber: number) {
    this.pensionService.receivePension(this.dataConfig.pd3Pensioner.referenceNumber, pensionNumber)
      .subscribe(() => {
        this.notify.openSnackBar("Pension received successfully", "", 'green');
        this.router.navigateByUrl("/pd3-pensions/pd3-list-view/pending");
      })

  }
}
