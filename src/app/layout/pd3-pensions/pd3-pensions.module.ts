import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { Pd3PensionsRoutingModule } from './pd3-pensions-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Pd3ListViewComponent } from './pd3-list-view/pd3-list-view.component';
import { ViewPensionsComponent } from './view-pensions/view-pensions.component';
import { ReceivePensionComponent } from './receive-pension/receive-pension.component';
import { ReceiveApplicationPopupComponent } from './popups/receive-application-popup/receive-application-popup.component';
import { ApplicationHistoryPopupComponent } from './popups/application-history-popup/application-history-popup.component';
import { DashboardTileComponent } from './dashboard-tile/dashboard-tile.component';
import { Pd3DetailedViewComponent } from './pd3-detailed-view/pd3-detailed-view.component';
import { Pd3PersonalInformationComponent } from './pd3-personal-information/pd3-personal-information.component';
import { Pd3DependentInformationComponent } from './pd3-dependent-information/pd3-dependent-information.component';
import { Pd3PensionInformationComponent } from './pd3-pension-information/pd3-pension-information.component';
import { EditPensionsComponent } from './components/edit-pensions/edit-pensions.component';
import { ContactDetailsPopupComponent } from './popups/contact-details-popup/contact-details-popup.component';
import { ViewPhotoPopupComponent } from './popups/view-photo-popup/view-photo-popup.component';
import { ExtraPaymentDetailsPopupComponent } from './popups/extra-payment-details-popup/extra-payment-details-popup.component';
import { ServiceDetailsPopupComponent } from './popups/service-details-popup/service-details-popup.component';
import { GratuityBankDetailsPopupComponent } from './popups/gratuity-bank-details-popup/gratuity-bank-details-popup.component';
import { DependentInformationPopupComponent } from './popups/dependent-information-popup/dependent-information-popup.component';
import { ViewGratuitiesComponent } from './components/view-gratuities/view-gratuities.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { StateChangeConfirmationPopupComponent } from './popups/state-change-confirmation-popup/state-change-confirmation-popup.component';
import { PrintLetterPopupComponent } from './popups/print-letter-popup/print-letter-popup.component';
import { RejectionPanelComponent } from './components/rejection-panel/rejection-panel.component';
import { FormsModule } from '@angular/forms';
import { AcceptGratuitiesPopupComponent } from './popups/accept-gratuities-popup/accept-gratuities-popup.component';
import { PostalReportComponent } from './reports/postal-report/postal-report.component';
import { ReportMainViewComponent } from './reports/report-main-view/report-main-view.component';
import { PostalDatepickerComponent } from './popups/postal-datepicker/postal-datepicker.component';
import { PrintSathkaraPopupComponent } from './popups/print-sathkara-popup/print-sathkara-popup.component';
import { AwardPrintComponent } from './components/award-print/award-print.component';
import { QuickAccessComponent } from './components/quick-access/quick-access.component';
import {SathkaraDailyReportComponent} from './reports/daily-report-sathkara/daily-report-sathkara.component'
import { DailyGratuityReportComponent } from './reports/daily-gratuity-report/daily-gratuity-report.component';
import { DailyRegistrationRecheckComponent } from './reports/daily-registration-recheck-report/daily-registration-recheck.component';

@NgModule({
  declarations: [
    DashboardComponent,
    Pd3ListViewComponent,
    ViewPensionsComponent,
    ReceivePensionComponent,
    ReceiveApplicationPopupComponent,
    ApplicationHistoryPopupComponent,
    DashboardTileComponent,
    Pd3DetailedViewComponent,
    Pd3PersonalInformationComponent,
    Pd3DependentInformationComponent,
    Pd3PensionInformationComponent,
    EditPensionsComponent,
    ContactDetailsPopupComponent,
    ViewPhotoPopupComponent,
    ExtraPaymentDetailsPopupComponent,
    ServiceDetailsPopupComponent,
    GratuityBankDetailsPopupComponent,
    DependentInformationPopupComponent,
    ViewGratuitiesComponent,
    StateChangeConfirmationPopupComponent,
    PrintLetterPopupComponent,
    RejectionPanelComponent,
    AcceptGratuitiesPopupComponent,
    PostalReportComponent,
    ReportMainViewComponent,
    PostalDatepickerComponent,
    PrintSathkaraPopupComponent,
    AwardPrintComponent,
    QuickAccessComponent,
    SathkaraDailyReportComponent,
    DailyGratuityReportComponent,
    DailyRegistrationRecheckComponent 
  ],
  entryComponents: [
    ReceiveApplicationPopupComponent,
    ApplicationHistoryPopupComponent,
    ContactDetailsPopupComponent,
    ViewPhotoPopupComponent,
    DependentInformationPopupComponent,
    ExtraPaymentDetailsPopupComponent,
    ServiceDetailsPopupComponent,
    GratuityBankDetailsPopupComponent,
    StateChangeConfirmationPopupComponent,
    PrintLetterPopupComponent,
    AcceptGratuitiesPopupComponent,
    PostalDatepickerComponent,
    PrintSathkaraPopupComponent
  ],
  imports: [
    CommonModule,
    Pd3PensionsRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatCheckboxModule,
    MatSelectModule,
    FormsModule,
  ]
})
export class Pd3PensionsModule { }
