import { TestBed } from '@angular/core/testing';

import { Pd3ViewPensionsService } from './pd3-view-pensions.service';

describe('Pd3ViewPensionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Pd3ViewPensionsService = TestBed.get(Pd3ViewPensionsService);
    expect(service).toBeTruthy();
  });
});
