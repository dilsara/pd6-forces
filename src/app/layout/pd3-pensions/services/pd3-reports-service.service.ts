import { Injectable } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';
import { API } from 'src/app/http/api';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { computeStyle } from '@angular/animations/browser/src/util';

@Injectable({
  providedIn: 'root'
})
export class Pd3ReportsServiceService {

  constructor(
    private configService: ConfigService,
    private api: API,
    private _http: HttpClient
  ) {}

  getPostalReport(date: string) {
    return this.configService.getWithToken(this.api.POSTAL_REPORT(date));
  }

  savePostalReportInFileServer(temparam: string, idparam: string, payload: any) {
    return this.configService.post(this.api.SAVE_IN_FILE_SERVER(temparam, idparam), payload)
  }

  authenticate() {

    let header: HttpHeaders = new HttpHeaders();
    header = header.append('Authorization', 'Basic amFzcGVyYWRtaW46cm9vdA==');

    return this._http.get<any>(`http://192.168.100.3:81/jasperserver/rest_v2/reports/reports/daily_report_from_sathkara_for_all.html?SELECTED_DATE=2019-03-05&CURRENT_DATE=2019-03-05`);
  }

  // getPostalReportSample(): Observable<any> {
  //   return this._http.get<any>(`${this.getPath()}reports/DataSources/postal_report_New.html`, { headers: { 'X-REMOTE-DOMAIN': '1', 'withCredentials': 'true' } });
  // }


}
