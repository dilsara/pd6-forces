import { Injectable } from '@angular/core';
import { API } from 'src/app/http/api';
import { ConfigService } from 'src/app/services/config.service';
import { RejectionModel } from '../models/rejection.model';
import { InterviewModel } from '../models/interview.model';
import { SMSModel } from 'src/app/models/sms.model';

@Injectable({
  providedIn: 'root'
})
export class Pd3PensionsService {

  constructor(
    private api: API,
    private configService: ConfigService
  ) { }

  // pd3 pension history
  getPd3PensionHistory(refNo: number, pensionNo: number) {
    return this.configService.getWithToken(this.api.VIEW_PD3_PENSION_HISTORY(refNo, pensionNo));
  }

  // pd3 pensioner details
  getPd3PensionersDetails(refNo: number) {
    return this.configService.getWithToken(this.api.GET_PENSIONER_DETAILS(refNo));
  }

  // receive a single pension
  receivePension(refNo: number, pensionNo: number) {
    return this.configService.getWithToken(this.api.RECEIVE_PD3_PENSIONER(refNo, pensionNo));
  }

  //get pd3 single pension details
  getPd3PensionDetails(refNumber: number, pensionNumber: number) {
    return this.configService.getWithToken(this.api.GET_PD3_PENSION_DETAILS(refNumber, pensionNumber));
  }

  //get pensioner's photo
  getPensionerPhoto(refNo: number) {
    return this.configService.getWithToken(this.api.GET_PENSIONER_PHOTO(refNo));
  }

  // get dependents array
  getPd3PensionerDependents(refNo: number) {
    return this.configService.getWithToken(this.api.GET_DEPENDENT_ARRAY(refNo));
  }

  // get single dependent
  getPd3PensionerSingleDependent(refNo: number, dependentId: number) {
    return this.configService.getWithToken(this.api.GET_SINGLE_DEPENDENT(refNo, dependentId));
  }

  //get consolidated salary 2020
  getConsolidateSalary2020(scale: string, grade: string, salary: number, circular: string, retired_date: string, increment_date: string) {
    return this.configService.getWithToken(
      this.api.GET_CONSOLIDATED_SALARY(scale, grade, salary, circular, retired_date, increment_date));
  }

  //reject application
  rejectApplication(refNo: number, pensionNumber: number) {
    return this.configService.getWithToken(this.api.REJECT_APPLICATION(refNo, pensionNumber));
  }

  // accept application
  acceptApplication(refNo: number, pensionNumber: number) {
    return this.configService.getWithToken(this.api.ACCEPT_APPLICATION(refNo, pensionNumber));
  }

  // retrieve all pd3 rejection reasons
  getpd3RejectReasons() {
    return this.configService.getWithToken(this.api.GET_ALL_REJECTION_REASONS());
  }

  // confirm & save rejection
  confirmRejection(rejection: RejectionModel) {
    return this.configService.put(this.api.SAVE_REJECTION(), rejection);
  }

  //print letter
  printLetter(refNo: number, data: InterviewModel) {
    return this.configService.put(this.api.PRINT_LETTER(refNo), data);
  }

  //get all pensions
  getPensions(refNo: number) {
    return this.configService.getWithToken(this.api.PD3_VIEW_PENSIONS(refNo));
  }

  //getExtra payment details
  getExtraPaymentDetails(refNo: number, pensionNo: number) {
    return this.configService.getWithToken(this.api.EXTRA_PAYMENT_DETAILS(refNo, pensionNo));
  }

  //search pd3 pensioners
  searchPensioner(refNo: number, name: string, nic: string, penNo: number) {
    return this.configService.getWithToken(this.api.PD3_SEARCH_PENSIONER(refNo, name, nic, penNo));
  }

  searchPensionerByNic(nic: string) {
    return this.configService.getWithToken(this.api.PD3_SEARCH_PENSIONER_BY_NIC(nic));
  }

  searchPensionerByName(name: string) {
    return this.configService.getWithToken(this.api.PD3_SEARCH_PENSIONER_BY_NAME(name));
  }

  searchPensionerByPenno(penNo: number) {
    return this.configService.getWithToken(this.api.PD3_SEARCH_PENSIONER_BY_PENNO(penNo));
  }

  getInterviewDetails(penNo: number) {
    return this.configService.getWithToken(this.api.GET_INTERVIEW_DETAILS(penNo));
  }

  sendInterviewSms(payload: SMSModel) {
    return this.configService.put(this.api.SEND_INTERVIEW_SMS(), payload);
  }
}
