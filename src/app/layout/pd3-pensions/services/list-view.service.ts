import { Injectable } from '@angular/core';
import { API } from 'src/app/http/api';
import { ConfigService } from 'src/app/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class ListViewService {

  constructor(
    private path: API,
    private serviceConfig: ConfigService
  ) { }


  // service call for list view pd3 pensioners
  getPD3ListView(state: string) {
    return this.serviceConfig.getWithToken(this.path.PD3LISTVIEW(state));
  }

}
