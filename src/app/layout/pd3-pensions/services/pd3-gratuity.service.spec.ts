import { TestBed } from '@angular/core/testing';

import { Pd3GratuityService } from './pd3-gratuity.service';

describe('Pd3GratuityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Pd3GratuityService = TestBed.get(Pd3GratuityService);
    expect(service).toBeTruthy();
  });
});
