import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivePensionComponent } from './receive-pension.component';

describe('ReceivePensionComponent', () => {
  let component: ReceivePensionComponent;
  let fixture: ComponentFixture<ReceivePensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceivePensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivePensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
