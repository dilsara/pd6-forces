export class PD3ListView {
    id: number;
    name: string;
    nic: string;
    dateOfRetired: any;
    wOPNumber: string;
    checked: boolean;

    constructor() {
        this.id = 0;
        this.name = "";
        this.dateOfRetired = "";
        this.nic = "";
        this.wOPNumber = "";
        this.checked = false;
    }
}