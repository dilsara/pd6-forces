export class InterviewDetailsModel {
    id: number;
    interviewDate: string;
    specialized: string;

    constructor() {
        this.id = 0;
        this.interviewDate = "";
        this.specialized = "";
    }
}