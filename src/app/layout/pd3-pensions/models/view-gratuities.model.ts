export class ViewGratuitiesModel {
    pensionerNumber: number;
    name: string;
    nic: string;
    accountNumber: string;
    gratuityAmount: number;
    checked: boolean = false;
    gratuityId: number;
}