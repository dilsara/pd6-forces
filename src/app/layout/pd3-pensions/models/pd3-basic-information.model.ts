export class PD3BasicInformationModel {
    salutation: string;
    name: string;
    address: string;
    nic: string;
    designation: string;
    dateOfRetired: string;
    dateOfBirth: string;
    pensionable: boolean;
    permanant: boolean;
    serviceDomain: string;
    wOPNumber: string;
    landNumber: string;
    mobileNumber: string;
    email: string;
    skype: string;
    grade: string;
    servicePeriod: ServicePeriod;
    salary: number;
    nextIncrementDate: string;
    firstAppoinmentDate: string;
    dateOfPermanantAppoinment: string;
    sectionUnder: string;
    retiredReason: string;
    trainedPeriod: string;
    wnopDeductedPeriod: string;
    permanantPeriod: string;
    gratuity: Gratuity;

    constructor() {
        this.salutation = "";
        this.name = "";
        this.address = "";
        this.nic = "";
        this.designation = "";
        this.dateOfRetired = "";
        this.dateOfBirth = "";
        this.pensionable = false;
        this.permanant = false;
        this.serviceDomain = "";
        this.wOPNumber = "";
        this.landNumber = "";
        this.mobileNumber = "";
        this.email = "";
        this.skype = "";
        this.grade = "";
        this.servicePeriod = new ServicePeriod();
        this.salary = 0.00;
        this.nextIncrementDate = "";
        this.firstAppoinmentDate = "";
        this.dateOfPermanantAppoinment = "";
        this.sectionUnder = "";
        this.retiredReason = "";
        this.trainedPeriod = "";
        this.wnopDeductedPeriod = "";
        this.permanantPeriod = "";
        this.gratuity = new Gratuity();
    }

}

class ServicePeriod {
    days: string;
    months: string;
    years: string;
}

class Gratuity {
    rel: string;
    url: string;
}