export class GratuityBankDetailsModel {

    id:number;
    accountNumber: string;
    gratuityAmount: number;
    govDeduction: number;
    netGratuityAmount: number;
    branch: Branch;
    pensioner: Pensioner;

    constructor() {
        this.accountNumber = "";
        this.gratuityAmount = 0.00;
        this.govDeduction = 0.00;
        this.netGratuityAmount = 0.00;
        this.branch = new Branch();
        this.pensioner = new Pensioner();
    }
}

class Branch {
    rel: string;
    url: string;
}

class Pensioner {
    rel: string;
    url: any;
}