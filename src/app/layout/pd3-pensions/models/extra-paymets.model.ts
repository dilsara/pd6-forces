export class ExtraPaymentDetails {
    amount: number;
    description: string;
    type: string;
    status: string;

    constructor() {
        this.amount = 0.00;
        this.description = "";
        this.type = "";
        this.status = "";
    }
}