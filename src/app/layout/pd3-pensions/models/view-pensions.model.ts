export class ViewPensionsModel {
    pensionNumber: number;
    type: string;
    pensionPoint: Office;
    circular: string;
    state: State;

    constructor() {
        this.pensionNumber = 0;
        this.pensionPoint = new Office();
        this.circular = "";
        this.state = new State();
        this.type = "";
    }

}

class Office {
    rel: string;
    url: string;
}

class State {
    rel: string;
    url: string;
}