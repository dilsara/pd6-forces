export class PD3PensionerModel {
    id: number;
    name: string;
    nic: string;
    designation: string;
    pensionerType: string;
    referenceNumber: number;
    wOPNumber: string;
    dateOfRetired: string;
    mobileNumber: string;

    constructor() {
        this.name = "";
        this.nic = "";
        this.designation = "";
        this.pensionerType = "";
        this.wOPNumber = "";
        this.dateOfRetired = "";
        this.mobileNumber = "";
        this.id = 0;
    }
}