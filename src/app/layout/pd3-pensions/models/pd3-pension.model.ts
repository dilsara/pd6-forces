export class PD3PensionModel {
    id: number;
    circular: string;
    scale: string;
    totalNoPay: TotalNoPay;
    netReducedPercentage: number;
    netUnreducedPercentage: number;
    consolidatedSalary: number;
    branch: Branch;
    accountNumber: string;

    constructor() {
        this.id = 0;
        this.circular = "";
        this.scale = "";
        this.totalNoPay = new TotalNoPay();
        this.netReducedPercentage = 0.0;
        this.netUnreducedPercentage = 0.0;
        this.consolidatedSalary = 0.00;
        this.branch = new Branch();
    }
}

class TotalNoPay {
    days: string;
    months: string;
    years: string;
}

class Branch {
    rel: string;
    url: string;
}