export class ConsolidatedSalary2020Model {
    basicSalary: number;
    firstAllowance: number;
    secondAllowance: number;
    step: number;

    constructor() {
        this.basicSalary = 0.00;
        this.firstAllowance = 0.00;
        this.secondAllowance = 0.00;
        this.step = 0;
    }
}