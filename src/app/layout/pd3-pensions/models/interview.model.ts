export class InterviewModel {
    interviewDate: string;
    likeToRejoin: boolean;
    specialized: string;
    id: number;

    constructor() {
        this.interviewDate = "";
        this.likeToRejoin = false;
        this.specialized = "";
        this.id = 0;
    }
}