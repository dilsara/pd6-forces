import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd3PensionInformationComponent } from './pd3-pension-information.component';

describe('Pd3PensionInformationComponent', () => {
  let component: Pd3PensionInformationComponent;
  let fixture: ComponentFixture<Pd3PensionInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd3PensionInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd3PensionInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
