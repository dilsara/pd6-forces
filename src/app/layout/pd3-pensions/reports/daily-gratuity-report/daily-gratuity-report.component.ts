import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { Pd3ReportsServiceService } from '../../services/pd3-reports-service.service';
import { DataConfigService } from '../../services/data-config.service';
import { ActivatedRoute, Params } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as $ from 'jquery';

@Component({
  selector: 'app-daily-gratuity-report',
  templateUrl: './daily-gratuity-report.component.html',
  styleUrls: ['./daily-gratuity-report.component.scss']
})
export class DailyGratuityReportComponent implements OnInit {

  public date: string;
  public fromDate;
  public toDate;
  public iframeURL :SafeResourceUrl;
  public iframeLoaded:boolean=false;
  public iframeclass="onframeload";
  public loaded=false;
  public loadedCount=0;
  constructor(
    public dataConfig: DataConfigService,
    private activatedRoute: ActivatedRoute,
    private reportService: Pd3ReportsServiceService,
    private sanitizer:DomSanitizer
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.date = params["date"]
    })
  }

   viewReport(){
    this.iframeURL = this.sanitizer.bypassSecurityTrustResourceUrl(`http://192.168.100.101:81/jasperserver/rest_v2/reports/reports/daily_report_gratuity_postal_pending.html?SELECTED_DATE=${this.fromDate}`);
    this.iframeLoaded=true;
    $('iframe').attr('src', $('iframe').attr('src'));
    this.loaded=true;
  }
  
  download(){
    this.changeStyle();
    window.open(`http://192.168.100.101:81/jasperserver/rest_v2/reports/reports/daily_report_gratuity_postal_pending.pdf?SELECTED_DATE=${this.fromDate}`,'this'); 
  }
  
  changeStyle(){
    if(this.loaded){
      this.iframeclass="afterframeload";
    }
   }
   
   onMyFrameLoad() {
    this.loadedCount+=1;
    if(this.loadedCount==2){
      this.changeStyle();
      this.loadedCount=0;
    }    
 }
}
