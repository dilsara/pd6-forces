import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { Pd3ReportsServiceService } from '../../services/pd3-reports-service.service';
import { DataConfigService } from '../../services/data-config.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-postal-report',
  templateUrl: './postal-report.component.html',
  styleUrls: ['./postal-report.component.scss']
})
export class PostalReportComponent implements OnInit {

  date: string;
  constructor(
    public dataConfig: DataConfigService,
    private activatedRoute: ActivatedRoute,
    private reportService: Pd3ReportsServiceService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.date = params["date"]
    })
  }

  public captureScreen() {
    var data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      var imgWidth = 190;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/jpeg')
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 10;
      pdf.addImage(contentDataURL, 'JPEG', 10, position, imgWidth, imgHeight);
      pdf.save('postal_report_' + this.date + '.pdf'); // Generated PDF
      // console.log(pdf.save('postal_report_' + this.date + '.pdf'));
      let payload = {
          "mime/type": "application/pdf",
          "valueImage": contentDataURL
      }
      this.reportService.savePostalReportInFileServer("13",this.date, payload)
        .subscribe(res => {
          console.log(res);
        })
    });
    // this.view();
  }

  view(){
    
    //Get the HTML of div
 let divElements = document.getElementById("contentToConvert").innerHTML;
 //Get the HTML of whole page
 var oldPage = document.body.innerHTML;
 
 // //Reset the page's HTML with div's HTML only
 document.body.innerHTML = divElements;
 // //To avoid extra blank page at the end
//  $("html, body").css("height", "auto");
//  Print Page
 window.print();
 
 // //Restore orignal HTML
 document.body.innerHTML = oldPage;
 console.log(oldPage);
 
   }
}
