import { WnopInfoModel } from "./wnop_info.model";

export class PersonalInfoModel {
    id : number;
    title:      string;
    fullName:   string ="GG";
    nic:        string;
    dob:        Date; 
    gender:     boolean;
    address:    Addresss;    
    mobile:     string;
    landNumber: string;
    email:string;
    passport :string;     
    district :number
    ds :number;
    gn :number;
    image :string;
}
 
export class Addresss {
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
}

export class ServiceInfoModel {
    persion_id              :number;
    wnop_number             :string;
    institute               :number;
    designation             :number;
    service                 :number; 
    salary_code             :number;     
    fist_appointment_date   :Date;
    appointment_date        :Date;     
    service_grade           :number;
}

export class DependentsInfoTblModel {

    fullName: string;
    relation: boolean; 
    dob: Date;
    nic: string;
    differentlyAabled: boolean;
    guardian:string;
    guardian_id :number;
    person_id :number ;
}
 

export class SpouseInfoTblModel {
    id : number;
    fullName: string;
    relation: boolean;
    // gender: string;
    dob: Date;
    nic: string; 
    birth_Cert_Number: number;
    marriage_Certificate_NO: number;
}


export class profileModel {
    personalInfo : PersonalInfoModel;
    serviceInfo: ServiceInfoModel; 
    dependants: DependentsInfoTblModel[]; 
    spouses: SpouseInfoTblModel[]; 
    marital_status: number; 
    wnopInfo:WnopInfoModel;
    
    
}