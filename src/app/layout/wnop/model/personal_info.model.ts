export interface PersonalInfoModel {
    id : number;
    title:      string;
    fullName:   string;
    nic:        string;
    dob:        Date; 
    gender:     boolean;
    address:    Address;    
    mobile:     string;
    landNumber: string;
    email:string;
    passport :string;     
    district :number
    ds :number
    gn :number
    image : string;
}
 
export interface Address {
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
}


