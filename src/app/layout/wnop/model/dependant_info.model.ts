import { SpouseInfoTblModel } from "./spouse_infoTb.model";
import { DependentsInfoTblModel } from "./dependant_infoTb.model";

export interface DependantsInfoModel {
    spouses: SpouseInfoTblModel[];
    dependants: DependentsInfoTblModel[];
    marital_status: number; 
}