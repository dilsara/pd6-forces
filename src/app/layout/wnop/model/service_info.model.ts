export interface ServiceInfoModel {
    persion_id              :number;
    wnop_number             :string;
    institute               :number;
    designation             :number;
    service                 :number; 
    salary_code             :number;     
    fist_appointment_date   :Date;
    appointment_date        :Date;     
    service_grade           :number;
}

