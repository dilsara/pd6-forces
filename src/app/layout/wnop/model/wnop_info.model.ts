export interface WnopInfoModel {
    person_id              :number;
    wnop_number            :string;
    old_wnop_number        :string;
    status                 :number;
    create_by              :number;
    create_date            :Date; 
    approved_date          :number; 
}
