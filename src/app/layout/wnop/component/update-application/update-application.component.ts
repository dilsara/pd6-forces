import { Component, OnInit, ViewChild } from '@angular/core';
import { SearchPersonComponent } from '../common/search-person/search-person.component';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ReRegistrationComponent } from '../re-registration/re-registration.component';
import { WnopService } from '../../services/wnop.service';
import { MasterDataService } from 'src/app/services/master-data.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-update-application',
  templateUrl: './update-application.component.html',
  styleUrls: ['./update-application.component.scss']
})
export class UpdateApplicationComponent implements OnInit { 

  @ViewChild(ReRegistrationComponent) profileDetail :ReRegistrationComponent;

  constructor(
    private dialogMgr: MatDialog,
    private service: WnopService,
    private mdService :MasterDataService,
    private activatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params:Params)=>{
      if( params["value"] =='0'){ 
        this.getSearchID();
      }
      else { 
        this.getDattaByID(params["value"]);
      }     
    })     
  }

  getDattaByID(id:number) {
    this.service.get(id).subscribe(
      response => {

        this.mdService.GET_PENSIONER_PHOTO("img-" + id.toString()).subscribe(
          image => {
            response.personalInfo.image = "data:"+ image["value"][0]["mime/type"] + ";base64," + image["value"][0]["valueImage"];  
            this.profileDetail.setModel(response);
            this.profileDetail.isEdit = true;
          })        
      },
      error => {
        // console.log(error);
      }
    );
  }

  getSearchID() {
    setTimeout(() => this.dialogMgr
      .open(SearchPersonComponent, {
      })
      .afterClosed()
      .subscribe((formValue: FormGroup) => {
        if (formValue && formValue.valid) {
          if (formValue.value.searchBy == 'id') {
            this.getDattaByID(formValue.value.value);
          }
        }
      }));
  }

}
