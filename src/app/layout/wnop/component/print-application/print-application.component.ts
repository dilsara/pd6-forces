import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddDependantComponent } from '../common/add-dependant/add-dependant.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SearchPersonComponent } from '../common/search-person/search-person.component';
import { ProfileDetailsComponent } from '../common/profile-details/profile-details.component';
import { ActivatedRoute, Params } from '@angular/router';
import { ProfileReport } from '../../reports/profile.Report';
import { WnopService } from '../../services/wnop.service';
import { MasterDataService } from 'src/app/services/master-data.service';
import { profileModel } from '../../model/profileModel';

@Component({
  selector: 'app-print-application',
  templateUrl: './print-application.component.html',
  styleUrls: ['./print-application.component.scss']
})
export class PrintApplicationComponent implements OnInit {

  form: FormGroup; 
  model:profileModel = null;

  @ViewChild(ProfileDetailsComponent) profileDetail :ProfileDetailsComponent;

  constructor(
    private dialogMgr: MatDialog,
    private formBuilder: FormBuilder,
    private activatedRoute:ActivatedRoute,
    private source :ProfileReport,
    private mdService :MasterDataService,
    private service: WnopService
  ) { }

  ngOnInit() {  
    this.form = this.formBuilder.group({
      personid : [0],
      status: [],
      changeStatus: [0, Validators.required],       
    }); 

    this.activatedRoute.params.subscribe((params:Params)=>{
      if( params["value"] =='0'){ 
        this.getSearchID() ;
      }
      else {
        this.form.get("personid").setValue(params["value"]); 
        this.getProfileModel(params["value"]);
      }     
    })    
  }

  getSearchID() {
    setTimeout(() =>this.dialogMgr
      .open(SearchPersonComponent, { 
      })
      .afterClosed()
      .subscribe((response: FormGroup) => {
        if (response && response.valid) {
          if(response.value.searchBy =='id') {
            this.form.get("personid").setValue(response.value.value);
            this.getProfileModel(response.value.value); 
          }  
        }
      }));
  }

  getProfileModel(id: number) { 
    this.service.get(id).subscribe(
      response => {  
        this.mdService.GET_PENSIONER_PHOTO("img-" +id).subscribe(
          image => {
            response.personalInfo.image = "data:"+ image["value"][0]["mime/type"] + ";base64," + image["value"][0]["valueImage"] 
          },
          error => {
            console.log(error);
          }
          )  
          this.model= response;  
          this.profileDetail.setModel(response);    
      },
      error => {
        console.log(error);
      }
    );
  }  

  changeStatus(id: number) {
    // console.log(this.getProfileModel('77'));
  }
 

  printSource() {
    this.source.print(this.model);
  }


  get district() { return this.form.get('district') }
}
