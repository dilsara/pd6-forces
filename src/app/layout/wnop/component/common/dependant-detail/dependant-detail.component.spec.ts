import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependantDetailComponent } from './dependant-detail.component';

describe('DependantDetailComponent', () => {
  let component: DependantDetailComponent;
  let fixture: ComponentFixture<DependantDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependantDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependantDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
