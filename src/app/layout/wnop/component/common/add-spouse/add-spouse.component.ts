import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NameValidator, ValidateForNic, ValidatenumberWithChar } from 'src/app/models/validators';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-spouse',
  templateUrl: './add-spouse.component.html',
  styleUrls: ['./add-spouse.component.scss']
})
export class AddSpouseComponent implements OnInit {

  form: FormGroup;
  maxDate: Date
  alert: any;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddSpouseComponent>,
    
  ) { 
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() - 1);
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id: [0],
      person_id: [0],
      fullName: ["", [Validators.required, NameValidator(), Validators.maxLength(300)]],
      relation: ["0", Validators.required],
      // gender: ["", Validators.required],
      dob: ["", Validators.required], 
      nic: ['', Validators.required, ValidateForNic.bind(this)],
      // disabled: [false, Validators.required],
      marriage_Certificate_NO: ["", [ValidatenumberWithChar()]],
      birth_Cert_Number: ["", [ ValidatenumberWithChar()]]
    });
 
  }

  close() {
    this.dialogRef.close();
  }

  saveAndClose() {
    if(this.form.valid) {
      this.dialogRef.close(this.form);
    
    } else {
      this.alert.show('Please Fill the Form Correctly');
    }
  }

}
