import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ValidationMessages } from 'src/app/models/validator.message';

@Component({
  selector: 'app-payment-iformation',
  templateUrl: './payment-iformation.component.html',
  styleUrls: ['./payment-iformation.component.scss']
})
export class PaymentIformationComponent implements OnInit {

  form: FormGroup; 
  isClickNext: Boolean;

  constructor(
    private formBuilder: FormBuilder,
    private validationMessages: ValidationMessages
  ) { }

  ngOnInit() {
    this.isClickNext = false;

    this.form = this.formBuilder.group({
      // fullName: ['', [Validators.required, NameValidator(), Validators.maxLength(300)]],
      // title: ['mr', Validators.required], 
      // nic: ['', Validators.required, ValidateForNic.bind(this)],
      // dob: ['', Validators.required],
      // gender: ['male', Validators.required],
      // addressLine1: ['', Validators.required],
      // addressLine2: [''],
      // addressLine3: [''],     
      // mobile: ['',[ Validators.required, ValidateForPhone(), Validators.maxLength(10)]],
      // landNumber: ['',[ValidateForPhone(), Validators.maxLength(15)]], 
      // email: ['',[emailValidator()]],
      // passport: ['',]     
    }); 
  }

}
