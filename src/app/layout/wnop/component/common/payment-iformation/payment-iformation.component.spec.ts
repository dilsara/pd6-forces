import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentIformationComponent } from './payment-iformation.component';

describe('PaymentIformationComponent', () => {
  let component: PaymentIformationComponent;
  let fixture: ComponentFixture<PaymentIformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentIformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentIformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
