import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalFileInputComponent } from './personal-file-input.component';

describe('PersonalFileInputComponent', () => {
  let component: PersonalFileInputComponent;
  let fixture: ComponentFixture<PersonalFileInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalFileInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalFileInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
