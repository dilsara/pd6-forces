import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NameValidator, ValidateForNic, ValidateForPhone, emailValidator } from 'src/app/models/validators';
import { PersonalInfoModel } from '../../../model/personal_info.model';
import { ValidationMessages } from 'src/app/models/validator.message';
import { MasterDataService } from 'src/app/services/master-data.service';
import { DistrictsModel } from '../../../model/districtsModel.model';
import { LoggerService } from 'src/app/services/logger.service'; 
import { DSModel } from '../../../model/DSModel.model';
import { GNModel } from '../../../model/GNModel.model';
import { NicList } from '../../../model/nicList.model';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {

  form: FormGroup; 
  isEdit = false;
  isClickNext: Boolean;
  districts: DistrictsModel[];
  dss: DSModel[];
  gns: GNModel[];

  nicList: NicList;

  url: string ='assets/images/User.jpg';

  constructor(
    private formBuilder: FormBuilder,
    private validationMessages: ValidationMessages,
    private mdService: MasterDataService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.isClickNext = false;
    this.form = this.formBuilder.group({
      id: [0],
      fullName: ['', [Validators.required, NameValidator(), Validators.maxLength(300)]],
      title: ['mr', Validators.required],
      nic: ['', Validators.required, ValidateForNic.bind(this)],
      dob: ['', Validators.required],
      gender: ['0', Validators.required],
      address: this.formBuilder.group({
        addressLine1: ['', Validators.required],
        addressLine2: [''],
        addressLine3: [''],
      }),
      mobile: ['', [Validators.required, ValidateForPhone(), Validators.maxLength(10)]],
      landNumber: ['', [ValidateForPhone(), Validators.maxLength(15)]],
      email: ['', [emailValidator()]],
      passport: [''],
      district: [null, Validators.required],
      ds: [null, Validators.required],
      gn: [null, Validators.required],
      image: [this.url, Validators.required],
    }); 
    this.initFormChangeHooks();
  }

  getModel(): PersonalInfoModel {
    return this.form.value;
  }

  setModel(model: PersonalInfoModel) {
    this.form.patchValue(model);
    this.url=  model.image;

  } 
  initFormChangeHooks() {

    //Get district list to UI 
    this.mdService.getDistrictList().subscribe(
      response => {
        this.districts = response;
      }, error => this.logger.log(error)
    );

    //Update DS list when change district
    this.district.valueChanges.subscribe(newValue => {
      this.mdService.getDsList(newValue).subscribe(response => {
        this.dss = response;
        this.gns = [];
      });
    }); 

    //Update GN list when change gn
    this.ds.valueChanges.subscribe(newValue => {
      this.mdService.getGnList(newValue).subscribe(response => {
        this.gns = response;
      });
    }); 
 
  }  

  nicExit(){
    this.mdService.checkNIC(this.form.get('nic').value).subscribe(response => {
          this.nicList = response; 
      }); 
   }

  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = event.target['result'];
        // this.form.get('image').setValue((event.target['result']).replace(/^data:[a-z]+\/[a-z]+;base64,/, "")) 
        this.form.get('image').setValue((event.target['result'])) 
      }
    }
  }

  get district() { return this.form.get('district') }
  get ds() { return this.form.get('ds') }
  get gn() { return this.form.get('gn') } 
  get fullName() { return this.form.get('fullName') }
  get addressLine1() { return this.form.get('addressLine1') }
  get addressLine2() { return this.form.get('addressLine2') }
  get addressLine3() { return this.form.get('addressLine3') }
  get name() { return this.form.get('name') }
}
