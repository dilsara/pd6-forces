import { Component, OnInit } from '@angular/core';
import { ServiceInfoModel } from '../../../model/service_info.model';
import { WnopInfoModel } from '../../../model/wnop_info.model';

@Component({
  selector: 'app-service-detail',
  templateUrl: './service-detail.component.html',
  styleUrls: ['./service-detail.component.scss']
})
export class ServiceDetailComponent implements OnInit {

  Servicemodel :ServiceInfoModel = null; 
  Wnopmodel :WnopInfoModel = null; 

  constructor() { }

  ngOnInit() {
  }

  setModel(Servicemodels: ServiceInfoModel,Wnopmodel :WnopInfoModel) {
    this.Servicemodel = Servicemodels;
    this.Wnopmodel = Wnopmodel; 
  }

}
