import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material'; 
import { FormGroup } from '@angular/forms';
import { PersonalFileInputComponent } from '../common/personal-file-input/personal-file-input.component';

@Component({
  selector: 'app-new-application',
  templateUrl: './new-application.component.html',
  styleUrls: ['./new-application.component.scss']
})
export class NewApplicationComponent implements OnInit {

  constructor(
    private dialogMgr: MatDialog
  ) { }

  ngOnInit() {
    this.getSearchID() ;
  }

  getSearchID() {
    setTimeout(() =>this.dialogMgr
      .open(PersonalFileInputComponent, { 
      })
      .afterClosed()
      .subscribe((response: FormGroup) => {
        if (response && response.valid) {
          if(response.value.searchBy =='id') { 
            
          }  
        }
      }));
  }

}
