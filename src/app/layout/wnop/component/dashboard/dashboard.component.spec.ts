import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WnopDashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
  let component: WnopDashboardComponent;
  let fixture: ComponentFixture<WnopDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WnopDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WnopDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
