import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { WnopService } from '../../services/wnop.service';
import { PersonListModel } from '../../model/personList.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class WnopDashboardComponent implements OnInit {

  displayedColumns: string[] = ['personId', 'name', 'nic', 'wnopNumber','tool'];
  dataSource = new MatTableDataSource<PersonListModel>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private service: WnopService,
    private router: Router
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;  

    this.service.getPersonList()
      .subscribe(data => { 
        this.dataSource = new MatTableDataSource<PersonListModel>(JSON.parse(JSON.stringify(data)));
        setTimeout(() => this.dataSource.paginator = this.paginator); 
      }, error => { 
      })
  }  

  viewPerson(personId:number) {
    this.router.navigate(['/wnop/print/'+personId]); 
  }

  editPerson(personId:number) {
    this.router.navigate(['/wnop/update/'+personId]); 
  }

}

