import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WnopRoutingModule } from './wnop-routing.module';
import { NewApplicationComponent } from './component/new-application/new-application.component';
import { ReRegistrationComponent } from './component/re-registration/re-registration.component';
import { WnopDashboardComponent } from './component/dashboard/dashboard.component';
import { MatTabsModule, MatIconModule, MatStepperModule, MatDialogModule, MatTable, MatTableModule, MatPaginatorModule } from '@angular/material';
import { PersonalInformationComponent } from './component/common/personal-information/personal-information.component';
import { ServiceInformationComponent } from './component/common/service-information/service-information.component';
import { PaymentIformationComponent } from './component/common/payment-iformation/payment-iformation.component'; 
import { SpouseIformationComponent } from './component/common/spouse-iformation/spouse-iformation.component'; 
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DependantInformationComponent } from './component/common/dependant-information/dependant-information.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { AddSpouseComponent } from './component/common/add-spouse/add-spouse.component';
import { AddDependantComponent } from './component/common/add-dependant/add-dependant.component';
import { ProfileDetailsComponent } from './component/common/profile-details/profile-details.component'; 
import { ServiceDetailComponent } from './component/common/service-detail/service-detail.component';
import { DependantDetailComponent } from './component/common/dependant-detail/dependant-detail.component';
import { PersonalDetailComponent } from './component/common/personal-detail/personal-detail.component';
import { DistrictTransformerPipe } from './pipes/district-transformar.pipe'; 
import { GenderTransfomarPipe } from './pipes/gender-transformar.Pipe';
import { SpouseRelationTransformarPipe } from './pipes/spose-relation-transformar.Pipe';
import { DependantRelationTransformarPipe } from './pipes/dependant-relation-transformar.Pipe';
import { YesNoTransfomarPipe } from './pipes/boolean-yesNo-transfomar.pipe';
import { MaritalStatusTransfomarPipe } from './pipes/marital-status-transformar.pipe';
import { GnTransformerPipe } from './pipes/gn-transformar.pipe';
import { DsTransformerPipe } from './pipes/ds-transformar.pipe';
import { PrintApplicationComponent } from './component/print-application/print-application.component';
import { SearchPersonComponent } from './component/common/search-person/search-person.component';
import { UpdateApplicationComponent } from './component/update-application/update-application.component';
import { PersonalFileInputComponent } from './component/common/personal-file-input/personal-file-input.component';
import { DesignationTransformerPipe } from './pipes/designation-transformar.pipe';
import { GradeTransformerPipe } from './pipes/grade-transformar.pipe';
import { PensionPointTransformerPipe } from './pipes/pension_point-transformar.pipe';
import { SalaryCodeTransformerPipe } from './pipes/salary_code-transformar.pipe';
import { ServiceTransformerPipe } from './pipes/service-transformar.pipe';
import { ProfileReport } from './reports/profile.Report';

const exportedComponents = [
  DistrictTransformerPipe,
  GnTransformerPipe,
  DsTransformerPipe,
  GenderTransfomarPipe,
  SpouseRelationTransformarPipe,
  DependantRelationTransformarPipe,
  YesNoTransfomarPipe,
  MaritalStatusTransfomarPipe,
  DesignationTransformerPipe,
  GradeTransformerPipe,
  PensionPointTransformerPipe,
  SalaryCodeTransformerPipe,
  ServiceTransformerPipe
];

@NgModule({
  declarations: [
    NewApplicationComponent, 
    ReRegistrationComponent, 
    WnopDashboardComponent, 
    PersonalInformationComponent, 
    ServiceInformationComponent, 
    PaymentIformationComponent, 
    SpouseIformationComponent, 
    DependantInformationComponent, 
    AddSpouseComponent, 
    AddDependantComponent, 
    ProfileDetailsComponent,  
    ServiceDetailComponent, 
    DependantDetailComponent, 
    PersonalDetailComponent,    
    ...exportedComponents, 
    PrintApplicationComponent, 
    SearchPersonComponent, 
    UpdateApplicationComponent, 
    PersonalFileInputComponent,
  ],
  imports: [
    CommonModule,
    WnopRoutingModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    
    //---Metirial
    MatTabsModule,
    MatIconModule,
    MatStepperModule,
    MatDialogModule, 
    MatTableModule,
    MatPaginatorModule
    
  ],  
  providers: [
    ProfileReport, 
  ],
  entryComponents: [
    AddDependantComponent,
    AddSpouseComponent,
    SearchPersonComponent,
    PersonalFileInputComponent
  ]
})
export class WnopModule { }
