import { Pipe, PipeTransform } from '@angular/core'; 

@Pipe({ name: 'yesNoT' })
export class YesNoTransfomarPipe implements PipeTransform {

    transform(value: string): string {
        return value == 'true' ? 'Yes' : 'No'
    };

}