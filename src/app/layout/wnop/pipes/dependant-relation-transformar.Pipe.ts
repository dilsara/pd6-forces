import { Pipe, PipeTransform } from '@angular/core'; 

@Pipe({ name: 'DependantRelationT' })
export class DependantRelationTransformarPipe implements PipeTransform {

    transform(value: number): string {
        return value == 0 ? 'Dauguther' : 'Son'
    };

}
