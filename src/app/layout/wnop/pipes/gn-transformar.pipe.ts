import { MasterDataService } from './../../../services/master-data.service';
import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'gnT' })
export class GnTransformerPipe implements PipeTransform {

    constructor(private service: MasterDataService) {}

    transform(id: number): Promise<string> {
        return new Promise((resolve, reject) => {
            this.service.getGn(id).subscribe(
                respo => {
                    resolve(respo.name);
                },
                err => reject(err)
            );
        })
    }
}