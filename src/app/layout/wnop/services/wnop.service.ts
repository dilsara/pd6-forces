import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; 
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/http/api'; 
import { profileModel } from '../model/profileModel';
import { PersonListModel } from '../model/personList.model';


@Injectable({
  providedIn: 'root'
})
export class WnopService { 

  constructor(
        private api: API,
        private http: HttpClient
  ) { 


  }

  createProfile(json: any): Observable<any> {
    return this.http.post<any>(this.api.BASE_PATH1 +'/profile',json);
  }

  updateProfile(json: any): Observable<any> {
    return this.http.put<any>(this.api.BASE_PATH1 +'/profile',json);
  }

  getPersonList(): Observable<PersonListModel> {
    return this.http.get<PersonListModel>(this.api.BASE_PATH1 +'/profile');
  }

  get(pesionID: number): Observable<profileModel> {
    // if(pesionID != 0) {
    //   return profile;
    // }
    // else{
    //   return this.http.get<any>(this.api.BASE_PATH1 + '/profile/' + pesionID);
    // }
    return this.http.get<any>(this.api.BASE_PATH1 + '/profile/' + pesionID);
  }
}
