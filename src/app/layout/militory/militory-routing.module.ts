import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IdentificationComponent } from './militory-components/identification/identification.component';
import { PersonalInformationComponent } from './militory-components/personal-information/personal-information.component';
import { ServiceInformationComponent } from './militory-components/service-information/service-information.component';
import { DependentInformationComponent } from './militory-components/dependent-information/dependent-information.component';
import { SearchPersonComponent } from './militory-components/search-person/search-person.component';
import { MainComponent } from './militory-components/main/main.component';
import { SubmittedComponent } from './militory-components/submitted/submitted.component';
import { PrintDocumentComponent } from './militory-components/print-document/print-document.component';

const routes: Routes = [
  { path: '', component: SearchPersonComponent },
  { path: 'main', component: MainComponent },
  { path: 'search-person', component: SearchPersonComponent },
  { path: 'identification', component: IdentificationComponent },
  { path: 'personal-information', component: PersonalInformationComponent },
  { path: 'service-information', component: ServiceInformationComponent },
  { path: 'dependent-information', component: DependentInformationComponent },
  { path: 'submit', component: SubmittedComponent },
  { path: 'print-document', component: PrintDocumentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MilitoryRoutingModule { }
