import { Injectable } from '@angular/core';
import { MilitoryAPI } from 'src/app/http/militory.api';
import { ConfigService } from 'src/app/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class PersonalInformationService {

  constructor(
    private api: MilitoryAPI,
    private config: ConfigService
  ) { }

  /**
   * get district list
   */
  getDistrictList() {
    return this.config.get(this.api.DISTRICT_LIST());
  }

  /**
   * get ds list
   * @param district_id 
   */
  getDsList(district_id: Number) {
    return this.config.get(this.api.DS_LIST(district_id));
  }

  /**
   * get gn list
   * @param ds_id 
   */
  getGnDivision(ds_id: Number) {
    return this.config.get(this.api.GN_DIVISION_LIST(ds_id));
  }

}
