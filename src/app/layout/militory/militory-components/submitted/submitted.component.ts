import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-submitted',
  templateUrl: './submitted.component.html',
  styleUrls: ['./submitted.component.scss']
})
export class SubmittedComponent implements OnInit {

  refNo: Number = 1717;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  printDoc() {
    this.router.navigateByUrl("/militory/print-document");
  }

}
