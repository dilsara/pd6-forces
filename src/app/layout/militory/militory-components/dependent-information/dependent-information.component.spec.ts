import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependentInformationComponent } from './dependent-information.component';

describe('DependentInformationComponent', () => {
  let component: DependentInformationComponent;
  let fixture: ComponentFixture<DependentInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependentInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependentInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
