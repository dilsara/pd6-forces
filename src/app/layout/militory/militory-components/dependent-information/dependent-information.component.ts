import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AddDependentComponent } from './add-dependent/add-dependent.component';
import { AddSpouseComponent } from './add-spouse/add-spouse.component';
import { DependantsInfoModel } from '../../models/dependent-information.model';
import { SpouseInfoTblModel } from '../../models/spouse-informarion-table.model';
import { DependentsInfoTblModel } from '../../models/dependent-information-table.model';
import { ValidationMessages } from '../../models/validator.message';

@Component({
  selector: 'app-dependent-information',
  templateUrl: './dependent-information.component.html',
  styleUrls: ['./dependent-information.component.scss']
})
export class DependentInformationComponent implements OnInit {
  DIALOG_WIDTH = '600px';
  spouses: SpouseInfoTblModel[] = [];
  dependents: DependentsInfoTblModel[] = [];
  
  form: FormGroup; 
  isClickNext: Boolean;
  isMarried: Boolean; 

  constructor(
    private formBuilder: FormBuilder,
    private validationMessages: ValidationMessages,
    private dialogMgr: MatDialog
  ) { }

  ngOnInit() {
    this.isClickNext = false;
    this.isMarried = false;

    this.form = this.formBuilder.group({
      marital_status: ['0', Validators.required]  
    }); 

    this.initFormChangeHooks();
  }

  addDependent() {
    this.dialogMgr
      .open(AddDependentComponent, {
        panelClass: "custom-dialog-container",
        data: this.spouses
      })
      .afterClosed()
      .subscribe((response: FormGroup) => {
        if (response && response.valid) {
          this.dependents.push(JSON.parse(JSON.stringify(response.value)));
        }
      });
  }

  initFormChangeHooks() {
    //Update DS list when change district
    this.marital_status.valueChanges.subscribe(newValue => { 
      if (newValue==0) {
        this.spouses.splice(0)
      }
    });
  }

  addSpouse() {
    this.dialogMgr
    .open(AddSpouseComponent, {
      width: this.DIALOG_WIDTH,
      panelClass: "custom-dialog-container",
    })
    .afterClosed()
    .subscribe((response: FormGroup) => {
      if(response && response.valid) {
        this.spouses.push(JSON.parse(JSON.stringify(response.value)));
      }
    })
  }

  removeDependant(row) {
    this.dependents.splice(row, 1);
  }

  removeSpouse(row) {
    this.spouses.splice(row, 1);
  }

  /**
   * get the from model
   */
  getModel(): DependantsInfoModel {
    return {
      spouses: this.spouses,
      dependants: this.dependents,
      marital_status: this.marital_status.value
    }
  }

  setModel(model: DependantsInfoModel) {
    this.marital_status.setValue(model.marital_status);
    this.spouses = model.spouses;
    this.dependents = model.dependants; 
  }

  get marital_status() { return this.form.get('marital_status')}
  get countofSpouse() { return this.spouses.length }
}
