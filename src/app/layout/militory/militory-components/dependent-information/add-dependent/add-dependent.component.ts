import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SpouseInfoTblModel } from '../../../models/spouse-informarion-table.model';

@Component({
  selector: 'app-add-dependent',
  templateUrl: './add-dependent.component.html',
  styleUrls: ['./add-dependent.component.scss']
})
export class AddDependentComponent implements OnInit {
  form: FormGroup;
  maxDate: Date;

  constructor(
    private formBuilder: FormBuilder,
    // private validationMessages: Gen55ValidationMessages,
    private dialogRef: MatDialogRef<AddDependentComponent>,
    @Inject(MAT_DIALOG_DATA) public spouses: SpouseInfoTblModel[],
  ) { 
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() - 1);
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      id : [0],
      person_id: [0],
      fullName: null,
      relation: [''], 
      dob: [''], 
      nic: [''],
      differentlyAabled: [false, Validators.required],
      guardian: [''],
      guardian_id :[0],
      // name: ['', [Validators.required, NameValidator()]],
      // relation: ['', Validators.required], 
      // dob: ['', Validators.required], 
      // nic: ['',  ValidateForNic.bind(this)],
      // differentlyAabled: [false, Validators.required],
      // guardian: ['', Validators.required]
    });

    this.initFormConfigurationHooks();
  }

  close() {
    this.dialogRef.close();
  }

  saveAndClose() {
    if(this.form.valid) {
      this.dialogRef.close(this.form);
    
    } else {
      //this.alert.show('Please Fill the Form Correctly');
    }
  }

  initFormConfigurationHooks() {
    if(this.spouses.length == 0) {
      this.form.get('guardian').disable();  
    }

  }

}
