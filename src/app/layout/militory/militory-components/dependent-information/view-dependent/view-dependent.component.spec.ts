import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDependentComponent } from './view-dependent.component';

describe('ViewDependentComponent', () => {
  let component: ViewDependentComponent;
  let fixture: ComponentFixture<ViewDependentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDependentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDependentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
