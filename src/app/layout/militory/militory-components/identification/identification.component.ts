import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import { Router } from '@angular/router';

export interface Organization {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.scss']
})
export class IdentificationComponent implements OnInit {

  organizations = ['Army','Navy','Air Force','Police'];
  types = ['Commissioned', 'Non-Commissioned', 'Civil'];
  regiments = ['A','B','C'];
  // organizations: Organization[] = [
  //   {value: 'steak-0', viewValue: 'Steak'},
  //   {value: 'pizza-1', viewValue: 'Pizza'},
  //   {value: 'tacos-2', viewValue: 'Tacos'}
  // ];
  

  identificationForm: FormGroup;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.identificationForm = new FormGroup({
      organization: new FormControl(),
      type: new FormControl(),
      soldierNumber: new FormControl(),
      regiment: new FormControl()
    });
  }

  
  /**
   * proceed button
   */
  identificationProceed(){
    this.router.navigateByUrl("/militory/personal-information");
  }

}
