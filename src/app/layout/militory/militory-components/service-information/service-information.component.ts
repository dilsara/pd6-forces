import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ServiceInformarionService } from '../../services/service-informarion.service';

@Component({
  selector: 'app-service-information',
  templateUrl: './service-information.component.html',
  styleUrls: ['./service-information.component.scss']
})
export class ServiceInformationComponent implements OnInit {

  ranks = ['Rank 1', 'Rank 2', 'Rank 3'];
  pensionableRanks = ['Rank 1', 'Rank 2', 'Rank 3'];
  regements = ['Regement 1', 'Regement 2', 'Regement 3'];
  units = ['Unit 1', 'Unit 2', 'Unit 3', 'Unit 4'];
  // salaryCodes = ['mn-1', 'mn-2', 'mn-7'];
  salaryCodes: any;
  salaryCirculars = ['2016', '2017', '2018'];
  // services = ['Associate Officer (Asso.Officer)','Casual(Casual)','Civil Security Service(CSecS)','Contract(Contract)'];
  services: any;
  // designations = ['Accountant','Addl. Chief Valuer','Administrative Officer'];
  designations: any;
  allowanceDiscriptions = ['Allowance 1', 'Allowance 2', 'Allowance 3'];
  deductionDiscriptions = ['Deduction 1', 'Deduction 2', 'Deduction 3'];
  salary2016circulars = [];


  allowances = ['Allowance 1', 'Allowance 2', 'Allowance 3', 'Allowance 4'];
  deductions = ['Deduction 1', 'Deduction 2', 'Deduction 3', 'Deduction 4'];

  serviceInfoForm: FormGroup;


  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private serviceInfoService: ServiceInformarionService
  ) { }

  ngOnInit() {
    this.serviceInfoForm = this.formBuilder.group({
      regementNo: ['', Validators.required],
      rank: ['', Validators.required],
      pensionableRank: ['', Validators.required],
      grade: ['', Validators.required],
      regement: ['', Validators.required],
      officialAddress: this.formBuilder.group({
        addressLine1: ['', Validators.required],
        addressLine2: ['', Validators.required],
        addressLine3: ['', Validators.required]
      }),
      soldierNo: ['', Validators.required],
      servicePenNo: ['', Validators.required],
      disabledPenNo: ['', Validators.required],
      unit: ['', Validators.required],
      appointmentDate: ['', Validators.required],
      pensionOrDeathDate: ['', Validators.required],
      pensionOrMedicalDate: ['', Validators.required],
      wopNo: ['', Validators.required],
      salaryCode: ['', Validators.required],
      salaryCircular: ['', Validators.required],
      service: ['', Validators.required],
      designation: ['', Validators.required]
    });
    this.getSalaryCodes();
    this.getServices();
    this.getDesignations();
    this.initFormChangeHooks();
  }

  /**
   * initial changes
   */
  initFormChangeHooks() {

  }

  /**
   * add allowance
   */
  addAllowance() {
    console.log('Add Allowance');
  }

  /**
   * add allowance
   */
  addDeduction() {
    console.log('Add Allowance');
  }

  add() { }

  /**
   * 
   * @param index 
   */
  deleteRowAllowance(index) {
    this.allowances.splice(index, 1);
  }

  /**
   * 
   * @param index 
   */
  deleteRowDeduction(index) {
    this.deductions.splice(index, 1);
  }

  /**
   * get salary code 
   */
  getSalaryCodes() {
    this.serviceInfoService.getSalaryCode().subscribe(data => {
      this.salaryCodes = data;
    }, error => {
      console.log("Salary code not found. " + error);
    });
    console.log(this.serviceInfoService.getSalaryCode());
    // this.serviceInfoService.getSalaryCode().subscribe(data => {
    //   this.services = data;
    // });
  }

  /**
  * get salary Circulars 
  */
  getSalaryCirculars() {
    // TODO enter service for get salary code
  }

  /**
  * get services 
  */
  getServices() {
    this.serviceInfoService.getServices().subscribe(data => {
      this.services = data;
    }, error => {
      console.log("Services not found. " + error);
    });
  }

  /**
  * get designations 
  */
  getDesignations() {
    this.serviceInfoService.getDesignation().subscribe(data => {
      this.designations = data;
    }, error => {
      console.log("Designations not found. " + error);
    });
  }

  /**
   * proceed button
   */
  serviceProceed() {
    this.router.navigateByUrl("/militory/dependent-information");
    // console.log('Service Information Proceed');
  }

  /**
   * previous button
   */
  servicePrevious() {
    this.router.navigateByUrl("/militory/personal-information");
  }

  getModel() {
    return this.serviceInfoForm.value;
  }

}
