import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-search-person',
  templateUrl: './search-person.component.html',
  styleUrls: ['./search-person.component.scss']
})
export class SearchPersonComponent implements OnInit {

  nic: string = '882892808V';
  nicValidation: boolean = true;

  searchPersonForm: FormGroup

  constructor(
    private router: Router,
    private notify:NotificationsService
  ) { }

  ngOnInit() {
    this.searchPersonForm = new FormGroup({
      nic: new FormControl()
    });
  }

  /**
   * Person Details Search Button
   */
  searchPerson() {
    // this.router.navigateByUrl('/miitory/personal-information');
    if (this.nic == '882892809V') {
      this.nicValidation = true;
      this.router.navigateByUrl("/militory/main");
    } else {
      // this.router.navigateByUrl("/militory/main");
      this.nicValidation = false;
      this.notify.openSnackBar("No details found for entered NIC.",'Close','');
    }
  }

  /**
   * New Person Details enter button
   */
  newPersonDetails(){
    this.router.navigateByUrl("/militory/main");
  }

}
