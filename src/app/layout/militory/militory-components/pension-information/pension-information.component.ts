import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { AllowanceModel } from '../../models/allowance.model';
import { PensionInformationModel } from '../../models/pension-information.model';

@Component({
  selector: 'app-pension-information',
  templateUrl: './pension-information.component.html',
  styleUrls: ['./pension-information.component.scss']
})
export class PensionInformationComponent implements OnInit {


  allowanceDiscriptions = ['Allowance 1', 'Allowance 2', 'Allowance 3'];
  deductionDiscriptions = ['Deduction 1', 'Deduction 2', 'Deduction 3'];
  // yearsGratuity: any = 0;
  // monthsGratuity: any = 0;

  allowances = [
    { description: 'Allowance 1', amount: '' },
    { description: 'Allowance 2', amount: '' },
    { description: 'Allowance 3', amount: '' },
    { description: 'Allowance 4', amount: '' }
  ];
  deductions = [
    { description: 'Deduction 1', amount: '' },
    { description: 'Deduction 2', amount: '' },
    { description: 'Deduction 3', amount: '' },
    { description: 'Deduction 4', amount: '' }
  ];

  retireReasons = [
    { reason: 'Over age 60' },
    { reason: 'Over age 50' },
    { reason: 'Over age 22' }
  ];

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      bank: [null, Validators.required],
      allowance: ['', Validators.required],
      retireReason: ['', Validators.required],
      countableServicePeriod: this.formBuilder.group({
        years: [0],
        months: [0],
        days: [0]
      }),
      uncountableServicePeriod: this.formBuilder.group({
        years: [0],
        months: [0],
        days: [0]
      }),
      basicSalary: [0.00, Validators.required],
      allowances: this.formBuilder.array([this.formBuilder.group({
        description: [''],
        amount: ['']
      })]),
      deductions: this.formBuilder.array([this.formBuilder.group({
        description: [''],
        amount: ['']
      })]),
      totalAllowance: [0.00, Validators.required],
      totalDeduction: [0.00, Validators.required],
      grossGratuity: [0.00, Validators.required],
      netGratuity: [0.00, Validators.required]
    });
    this.initFormChangeHooks();
  }

  getModel(): PensionInformationModel {
    return this.form.value;
  }

  /**
   * 
   */
  initFormChangeHooks() {
    // this.RetireReason.valueChanges.subscribe(newValue => {
    //   console.log(this.form.get('retireReason').value);
    // });
    this.CountableServicePeriod.valueChanges.subscribe(newValue => {
      // console.log(this.form.get('countableServicePeriod').get('years').value);
    });
    this.BasicSalary.valueChanges.subscribe(newValue => {
      this.calculateGratuity();
    });
    // this.BasicSalary.valueChanges.subscribe(newValue => {
    //   this.calculateGratuity();
    // });
  }


  /**
   * Add allowance Row
   */
  addAllowance() {
    this.getAllowance.push(this.formBuilder.group({ description: '', amount: '' }));
  }

  /**
   * Remove Allowance row
   * @param index 
   */
  removeAllowance(index) {
    this.getAllowance.removeAt(index);
    this.calculateGratuity();
  }

  /**
   * add allowance
   */
  addDeduction() {
    this.getDeduction.push(this.formBuilder.group({ description: '', amount: '' }));
  }

  /**
   * Remove Allowance row
   * @param index 
   */
  removeDeduction(index) {
    this.getDeduction.removeAt(index);
    this.calculateGratuity();
  }


  // Gratuity calculation
  calculateGratuity() {
    this.calculateTotalAlllowance();
    this.calculateTotalDeduction();

    let basicSalary = this.form.get('basicSalary').value;
    let totalAllowances = this.form.get('totalAllowance').value;
    let totalDeductions = this.form.get('totalDeduction').value;

    let grossGratuity = basicSalary + totalAllowances;
    this.form.get('grossGratuity').setValue(grossGratuity.toFixed(2));

    let countableServiceYears = this.CountableServicePeriod.get('years').value;
    let grossSalaryYears = grossGratuity * countableServiceYears;

    let countableServiceMonths = this.CountableServicePeriod.get('months').value;

    let grossSalaryMonths = (grossGratuity * countableServiceMonths) / 12;

    let netGratuity = grossSalaryYears + grossSalaryMonths - totalDeductions;

    this.form.get('netGratuity').setValue(netGratuity);
    console.log(this.form.get('netGratuity').value);
  }


  calculateTotalAlllowance() {
    let alowances = this.form.get('allowances').value;
    let totalAllowance = 0;
    alowances.forEach(element => {
      console.log(element.amount);
      totalAllowance = totalAllowance + element.amount;
    });
    this.form.get('totalAllowance').setValue(totalAllowance);

    return this.form.get('totalAllowance').value;
  }

  calculateTotalDeduction() {
    let deductions = this.form.get('deductions').value;
    let totalDeduction = 0;
    deductions.forEach(element => {
      console.log(element.amount);
      totalDeduction = totalDeduction + element.amount;
    });
    this.form.get('totalDeduction').setValue(totalDeduction);

    return this.form.get('totalDeduction').value;
  }


  // calculateTotalDeduction() {
  //   let deductions = this.form.get('deductions').value;
  //   let totalDeduction = 0;
  //   deductions.forEach(element => {
  //     console.log(element.amount);
  //     totalDeduction = totalDeduction + element.amount;
  //   });
  //   this.form.get('totalDeduction').setValue(totalDeduction);

  //   return this.form.get('totalDeduction').value;
  // }


  // get models
  get RetireReason() {
    return this.form.get('retireReason')
  }
  get BasicSalary() {
    return this.form.get('basicSalary')
  }
  get CountableServicePeriod() {
    return this.form.get('countableServicePeriod')
  }
  get UnCountableServicePeriod() {
    return this.form.get('uncountableServicePeriod')
  }
  get allowance() {
    return this.form.get('allowances')
  }

  get getAllowance() {
    return this.form.get('allowances') as FormArray;
  }

  get getDeduction() {
    return this.form.get('deductions') as FormArray;
  }

}
