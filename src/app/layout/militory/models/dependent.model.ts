export class DependentModel {
    dependent: Dependents[];
}

class Dependents {
    dependentName: string;
    relationship: string;
    dependentNic: string;
    dependentDob: string;
    dependentAwardNo: string;
    dependentAddress: string;
    dependentStatus: string;
    dependentMaritialStaus: string;
    dependentBank: string;
    dependentBranch: string;
    dependentAccNo: Number;
}