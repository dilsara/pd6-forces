import { SpouseInfoTblModel } from "./spouse-informarion-table.model";
import { DependentsInfoTblModel } from "./dependent-information-table.model";

export interface DependantsInfoModel {
    spouses: SpouseInfoTblModel[];
    dependants: DependentsInfoTblModel[];
    marital_status: string; 
}