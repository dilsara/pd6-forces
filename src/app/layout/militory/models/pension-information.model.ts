import { AnimationDurations } from "@angular/material";
import { TimePeriod } from "./time-period.model";
import { AllowanceModel } from "./allowance.model";
import { DeductionModel } from "./deduction.model";

export class PensionInformationModel {
    bank: string;
    retireReason: string;
    countableServicePeriod: TimePeriod;
    uncountableServicePeriod: TimePeriod;
    basicSalary: Number;
    allowance: AllowanceModel[];
    deduction: DeductionModel[];
    totalAllowance: Number;
    totalDeduction: Number;
    grossGratuity: Number;
    netGratuity: Number;
}
