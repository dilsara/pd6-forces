export class TimePeriod{
    years: Number;
    months: Number;
    days: Number;
}