import { FormControl } from '@angular/forms';

export class ValidationMessages {
    pattern(name: string): string {
        return 'Please enter a valid ' + name + '.';
    }

    maxlength(controller: FormControl): string {
        console.log(controller); 
         return 'Maximum Length Exceeded.';
    }

    minlength(controller: FormControl): string {
        return 'Minimum ' + controller.errors.minlength.requiredLength + ' charactors are allowed.';
    }

    required(name: string): string {
        return name + ' is required.';
    }

    mismatch(name: string): string {
        return 'Please make sure ' + name + ' are match.';
    }

    other(message: string): string {
        return message;
    }
}