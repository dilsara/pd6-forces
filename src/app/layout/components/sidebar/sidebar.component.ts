import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Pd3SideBarConfig } from 'src/app/models/pd3-sidebar-config.model';
import { wnopSideBarConfig } from 'src/app/models/wnop-sidebar-config.model'
import { Pd6SideBarConfig } from 'src/app/models/pd6-sidebar-config.model';
import { MilitorySideBarConfig } from 'src/app/models/militory-sidebar-config.model';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    isActive: boolean;
    collapsed: boolean;
    showMenu: string;
    pushRightClass: string;

    public pd3sideBarConfig: Pd3SideBarConfig = new Pd3SideBarConfig(); // pd3 sidebar configuration
    public wnopSideBarConfig: wnopSideBarConfig = new wnopSideBarConfig(); // wop sidebar configuration
    public pd6SideBarConfig: Pd6SideBarConfig = new Pd6SideBarConfig();// pd6 sidebar configuration
    public militorySideBarConfig: MilitorySideBarConfig = new MilitorySideBarConfig(); // militory sidebar configuration
    @Output() collapsedEvent = new EventEmitter<boolean>();

    constructor(private translate: TranslateService, public router: Router) {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.isActive = false;
        this.collapsed = false;
        this.showMenu = '';
        this.pushRightClass = 'push-right';
        this.pd3sideBarConfig.getSideBarConfig(localStorage.getItem("userRole"));
        this.wnopSideBarConfig.getSideBarConfig(localStorage.getItem("userRole"));
        // this.pd6SideBarConfig.getSideBarConfig(localStorage.getItem("role"));
        this.pd6SideBarConfig.getSideBarConfig(localStorage.getItem("role"));
        this.militorySideBarConfig.getSidebarConfig(localStorage.getItem("userRole"));
    }


    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }
}
