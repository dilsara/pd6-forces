export class FillPaymentModel {
  /**
   * subpaths
   * @author Shageesha Prabagaran
   */

  circular: String;
  circular_revised: String;
  salary_scale: number;
  service_grade: number;
  basic_salary: number;
  reduce_percentage_revised: number;
  unreduced_percentage_revised: number;
  reduce_percentage_2020: number;
  unreduced_percentage_2020: number;
  increment_date: String;
  total_service_days?: number = 0;
  total_service_months?: number = 0;
  total_service_years?: number = 0;
  reduced_salary?: number;
  unreduced_salary?: number;
  servicperiod_years?: number;
  servicperiod_months?: number;
  servicperiod_days?: number;
  nopayperiod_years?: number;
  nopayperiod_months?: number;
  nopayperiod_days?: number;
  allowance_desc?: String;
  allowance_amount?: number;
  allowance_tot?: number = 0;
  nonallowance_tot?: number = 0;
  section?: String;
  increment2015: number;
  increment2017: number;
} 