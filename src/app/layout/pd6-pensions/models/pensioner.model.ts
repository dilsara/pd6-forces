export class PensionerModel {

    /**
 * subpaths
 * @author Shageesha Prabagaran
 */

    active?: number;
    ad1?: String;
    ad2?: String;
    ad3: String;
    bac: String;
    bnno: number;
    bpen: number;
    brno: number;
    cla: number;
    dob: String;
    dscode: number;
    id: number;
    maa: number;
    mobile: String;
    name: String;
    nic: String;
    ota: number;
    penno: String;
    phone: String;
    pname: String;
    pnet: number;
    pt: number;
    rdate: String;
    spa: number;
    sul: number;
    tded: number;
    tpen: number;
}