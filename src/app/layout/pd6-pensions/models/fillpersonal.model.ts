export class FillPersonalModel {

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */
    
    title?: String;
    gender?: number;
    retired_reason?: String;
    retired_section?: String;
    nopay_days?: number;
    nopay_months?: number;
    nopay_years?: number;
    designation?: number;
    service?: String;
    district?: String;
    division?: String;
    gramaniladhari?: String;
    dscode?: number;
    id?: number;
    pensionid?: String;
}