export class PensionModel {

    /**
     * subpaths
     * @author Shageesha Prabagaran
     */

    personalInfo?: PersonalInfo;
    pension_service?: PensionService;
    pension?: Pension;
    revision?: Revision;
    allowances?: Allowances;

    constructor() {
        this.personalInfo = new PersonalInfo();
        this.pension_service = new PensionService();
        this.pension = new Pension();
        this.revision = new Revision();
        this.allowances = new Allowances();
    }
}

export class PersonalInfo {
    title?: String;
    fullName?: String;
    gender?: number;
    dob?: String;
    address?: Address;
    nic?: String;
    mobile?: String;
    landNumber?: String;
    gn?: number;
    id?: number;
    dscode?: number;

    constructor() {
        this.address = new Address();
        this.title = "";
        this.fullName = "";
        this.gender = 0;
        this.dob = "";
        this.nic = "";
        this.mobile = "";
        this.landNumber = "";
        this.gn = 0;
        this.dscode = 0;
    }
}

export class PensionService {
    retired_date: String;
    retired_reason: String;
    section: String;
    nopay_days: number;
    nopay_months: number;
    nopay_years: number;
    service_grade: number;
    salary_scale: number;
    total_service_days: number;
    total_service_months: number;
    total_service_years: number;
    net_service_days: number;
    net_service_months: number;
    net_service_years: number;
    designation: number;
    service: String;

    constructor() {
        this.retired_date = "";
        this.retired_reason = "";
        this.section = "";
        this.nopay_days = 0;
        this.nopay_months = 0;
        this.nopay_years = 0;
        this.service_grade = 0;
        this.salary_scale = 0;
        this.total_service_days = 0;
        this.total_service_months = 0;
        this.total_service_years = 0;
        this.designation = 0;
        this.service = "";
    }
}

export class Pension {
    id: String;
    pension_type: number;
    circular: String;
    increment_date: String;
    net_reduced_percentage: number;
    net_unreduced_percentage: number;
    basic_salary: number;
    reduced_salary: number;
    unreduced_salary: number;
    total_amount_paid: number;
    earned_increment_2015: number;
    earned_increment_2017: number;

    constructor() {
        this.id = "";
        this.pension_type = 0;
        this.circular = "";
        this.net_reduced_percentage = 0;
        this.net_unreduced_percentage = 0;
        this.basic_salary = 0;
        this.reduced_salary = 0;
        this.unreduced_salary = 0;
        this.increment_date = "";
        this.total_amount_paid = 0;
        this.earned_increment_2015 = 0;
        this.earned_increment_2017 = 0;
    }
}

export class Revision {
    id: number;
    pid: number;
    salary_scale: number;
    reduced_salary_revised: number;
    unreduced_salary_revised: number;
    circular_revised: String;
    reduce_percentage_revised: number;
    unreduced_percentage_revised: number;
    reduce_percentage_2020: number;
    unreduced_percentage_2020: number;
    prev_salary_scale: number;
    prev_basic_salary: number;
    prev_circular: String;
    prev_reduced_salary: number;
    prev_unreduced_salary: number;
    prev_reduced_percentage: number;
    prev_unreduced_percentage: number;
    salary_increase: number;

    constructor() {
        this.salary_scale = 0;
        this.reduced_salary_revised = 0;
        this.unreduced_percentage_revised = 0;
        this.circular_revised = "";
        this.reduce_percentage_revised = 0;
        this.unreduced_percentage_revised = 0;
        this.reduce_percentage_2020 = 0;
        this.unreduced_percentage_2020 = 0;
        this.prev_salary_scale = 0;
        this.prev_basic_salary = 0;
        this.prev_circular = "";
        this.prev_reduced_salary = 0;
        this.prev_unreduced_salary = 0;
        this.prev_reduced_percentage = 0;
        this.prev_unreduced_percentage = 0;
        this.salary_increase = 0;
    }
}

export class Allowances {
    data?: Array<Data>;
}

export class Data {
    description?: String;
    amount?: number;
    status?: String;
    is_pensionable?: boolean;

    constructor() {
        this.description = "";
        this.amount = 0;
        this.status = "";
        this.is_pensionable = true;
    }
}


export class Address {
    addressLine1?: String;
    addressLine2?: String;
    addressLine3?: String;

    constructor() {
        this.addressLine1 = "";
        this.addressLine2 = "";
        this.addressLine3 = "";
    }
}
