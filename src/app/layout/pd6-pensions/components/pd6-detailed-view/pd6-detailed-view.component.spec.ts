import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd6DetailedViewComponent } from './pd6-detailed-view.component';

describe('Pd6DetailedViewComponent', () => {
  let component: Pd6DetailedViewComponent;
  let fixture: ComponentFixture<Pd6DetailedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd6DetailedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd6DetailedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
