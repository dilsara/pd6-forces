import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PensionerService } from '../../../services/pensioner.service';
import { DataConfigService } from '../../../dataconfig/data-config.service';
import { PensionerModel } from '../../../models/pensioner.model';
import { PensionModel } from '../../../models/pension.model';
import { FillPaymentModel } from '../../../models/fillpayment.model';
import { forEach } from '@angular/router/src/utils/collection';
import { element } from '@angular/core/src/render3';
import { NotificationsService } from 'src/app/services/notifications.service';

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

@Component({
	selector: 'app-view-payment-info',
	templateUrl: './view-payment-info.component.html',
	styleUrls: ['./view-payment-info.component.scss']
})
export class ViewPaymentInfoComponent implements OnInit {

	form: FormGroup;
	pensiondetail: PensionerModel;
	type = ""; offeredTo: any;
	percentage = [];
	reducedCal: number = null;
	unreducedCal: number = null;
	reduced_perc = null;
	unreduced_perc = null;
	scalename = "";
	gradename = "";
	basic_rev = null;
	rev_reduce_perc = null;
	rev_unreduce_perc = null;
	rev_reduce_salary: number = 0;
	rev_unreduce_salary: number = 0;
	current_annual_consolidated_salary = null;
	cuurent_gross_salary = null;
	sequenceNumber = 1;
	allowance_description;
	nonallowance_description;
	nonallowance_initialamount;
	allowance_amount: number = 0;
	nonallowance_amount: number = 0;
	allarray = [];
	nonallarray = [];
	fixcurrentincre = null;
	fixrevincre = null;
	fixreducedsal = null;
	fixunreducedsal = null;
	fixedunrevreduced = null;
	fixedrevreduced = null;
	today: number = 0;
	difference: number = 0;
	incre_date: number = 0;
	totalpensiontobepaid = null;
	todayfix: number = 0;
	unreduce: number = 0;
	rdatenew: any; salaryincrease: any; particular_year: any; particular_month: any;
	gap: number = 0; newreduced: any; date2: any; date1: any; diff: any; cstate: any;
	ptSorted: any; bpenVariance: any; reducedSal2017Fixed: any; widowList: any;


	fillpayment: FillPaymentModel = {
		circular: "",
		circular_revised: "",
		salary_scale: null,
		service_grade: null,
		basic_salary: null,
		reduce_percentage_revised: null,
		unreduced_percentage_revised: null,
		reduce_percentage_2020: null,
		unreduced_percentage_2020: null,
		increment_date: "",
		reduced_salary: null,
		unreduced_salary: null,
		servicperiod_years: 0,
		servicperiod_months: 0,
		servicperiod_days: 0,
		nopayperiod_years: 0,
		nopayperiod_months: 0,
		nopayperiod_days: 0,
		allowance_desc: "",
		allowance_amount: null,
		allowance_tot: 0,
		nonallowance_tot: 0,
		section: "",
		increment2015: null,
		increment2017: null
	};

	public scalelist: Array<any> = [
		{ "id": "27", "name": "or-1-group-I" },
		{ "id": "28", "name": "or-1-group-II" },
		{ "id": "29", "name": "or-1-group-III" },
		{ "id": "30", "name": "or-2-group-I" },
		{ "id": "31", "name": "or-2-group-II" },
		{ "id": "32", "name": "or-2-group-III" },
		{ "id": "33", "name": "or-2-group-sp" },
		{ "id": "34", "name": "or-3-group-I" },
		{ "id": "35", "name": "or-3-group-II" },
		{ "id": "36", "name": "or-3-group-III" },
		{ "id": "37", "name": "or-3-group-sp" },
		{ "id": "38", "name": "or-4-group-I" },
		{ "id": "39", "name": "or-4-group-II" },
		{ "id": "40", "name": "or-4-group-III" },
		{ "id": "41", "name": "or-4-group-sp" },
		{ "id": "42", "name": "or-5-group-I" },
		{ "id": "43", "name": "or-5-group-II" },
		{ "id": "44", "name": "or-5-group-III" },
		{ "id": "45", "name": "or-5-group-sp" },
		{ "id": "46", "name": "or-6-group-I" },
		{ "id": "47", "name": "or-6-group-II" },
		{ "id": "48", "name": "or-6-group-III" },
		{ "id": "49", "name": "or-6-group-sp" },
		{ "id": "50", "name": "or-6a-group-II" },
		{ "id": "51", "name": "or-6a-group-III" },
		{ "id": "52", "name": "or-6a-group-sp" },
		{ "id": "53", "name": "or-7-group-I" },
		{ "id": "54", "name": "or-7-group-II" },
		{ "id": "55", "name": "or-7-group-III" },
		{ "id": "56", "name": "or-7-group-sp" },
		{ "id": "62", "name": "r-1" },
		{ "id": "64", "name": "r-2" },
		{ "id": "65", "name": "r-3" },
		{ "id": "66", "name": "r-4" },
		{ "id": "67", "name": "r-5" },
		{ "id": "68", "name": "r-6" },
		{ "id": "69", "name": "r-7" },
		{ "id": "70", "name": "r-8" },
		{ "id": "71", "name": "r-9" },
		{ "id": "63", "name": "r-10" },
		{ "id": "100", "name": "r-1-2006-a" },
		{ "id": "83", "name": "sl-1" },
		{ "id": "84", "name": "sl-2" },
		{ "id": "85", "name": "sl-3" },
		{ "id": "86", "name": "sl-4" },
		{ "id": "87", "name": "sl-5" },
		{ "id": "107", "name": "sl-7" }
	]

	public gradelist: Array<any> = [
		{ "id": "1", "name": "grade-3-Ia", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "2", "name": "grade-3-Ib", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "3", "name": "grade-3-Ic", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "4", "name": "grade-3-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "5", "name": "grade-2-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "6", "name": "grade-2-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "7", "name": "grade-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "8", "name": "grade-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "9", "name": "grade-I-postal", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "10", "name": "grade-II-postal", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "11", "name": "special-grade-postal", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "12", "name": "grade-III", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "13", "name": "special-grade", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "14", "name": "pc", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "15", "name": "ps", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "16", "name": "si", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "17", "name": "sm", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "18", "name": "ip", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "19", "name": "cip", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "20", "name": "dentist", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "21", "name": "medical-officer-grade-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "22", "name": "medical-officer-grade-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "23", "name": "preliminary-grade", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "24", "name": "adg", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "25", "name": "adl", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "26", "name": "ald", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "27", "name": "asa", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "28", "name": "ddg", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "29", "name": "dld", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "30", "name": "dsg", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "31", "name": "sa", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "32", "name": "sald", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "33", "name": "sasa", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "34", "name": "sc", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "35", "name": "ssc", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "36", "name": "group-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "37", "name": "group-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "38", "name": "group-III", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "39", "name": "group-sp", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "40", "name": "ps-grade-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "41", "name": "pc-grade-III", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "42", "name": "slts-3-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "43", "name": "graduate-teacher", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "44", "name": "slts-1", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "45", "name": "slts-2-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "46", "name": "slts-2-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "47", "name": "slts-3-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "48", "name": "trained-teacher", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "49", "name": "slps-1", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "50", "name": "slps-2-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "51", "name": "slps-2-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "52", "name": "slps-3", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "53", "name": "A", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "54", "name": "B", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "55", "name": "C", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "56", "name": "D", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "57", "name": "E", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "58", "name": "F", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "59", "name": "G", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "60", "name": "H", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "61", "name": "I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "62", "name": "IA", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "63", "name": "J", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "64", "name": "K", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "65", "name": "L", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "66", "name": "M", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "67", "name": "N", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "68", "name": "supra", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "69", "name": "MO-specialists", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "70", "name": "Senior-Executive", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "71", "name": "slts-3-Ia", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "72", "name": "slts-3-Ib", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "73", "name": "slts-3-Ic", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "74", "name": "slts-3-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "75", "name": "slts-2-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "76", "name": "slts-2-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "77", "name": "slts-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "78", "name": "slps-2", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "79", "name": "chief-custom-inspect", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "80", "name": "custom-inspect-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "81", "name": "custom-inspect-II", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "82", "name": "custom-inspect-I", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" },
		{ "id": "83", "name": "no-grade", "create_by": null, "create_date": "2019-02-28 03:34:45", "update_by": null, "update_date": "0000-00-00 00:00:00" }

	];

	public allowancelist: Array<any> = [
		{ "allowance_id": "3", "special": "0", "description": "Personal Allowances  25%", "type": "allowance" },
		{ "allowance_id": "5", "special": "0", "description": "Pensionable allowance for the postal employee", "type": "earned increment" },
		{ "allowance_id": "6", "special": "0", "description": "Personal Allowances 50%", "type": "allowance" },
		{ "allowance_id": "8", "special": "0", "description": "Pensionable allowances for  the postal employees", "type": "allowance" },
		{ "allowance_id": "9", "special": "0", "description": "Pensionable allowances for  the doctors", "type": "allowance" },
		{ "allowance_id": "20", "special": "0", "description": "Adjustable Allowances", "type": "allowance" },
		{ "allowance_id": "26", "special": "0", "description": "Ration Allowance", "type": "allowance" },
		{ "allowance_id": "27", "special": "0", "description": "Qualification Pay", "type": "allowance" },
		{ "allowance_id": "28", "special": "0", "description": "Re-engagement Pay", "type": "allowance" },
		{ "allowance_id": "7", "special": "0", "description": "Good Conduct", "type": "allowance" },
		{ "allowance_id": "30", "special": "0", "description": "Administrative Allowances", "type": "allowance" }
	]

	public sectionlist: Array<any> = [
		{ "name": "Section 2:17" },
		{ "name": "Section 2:14" },
		{ "name": "Section 2:17(26)IX" },
		{ "name": "Section 2:7" },
		{ "name": "Section 2:25" },
		{ "name": "Section 2:48" },
		{ "name": "Public administration circular 44/90" },
		{ "name": "Public administration circular 14/33" },
		{ "name": "WNOP-Death Gratuity" },
		{ "name": "PAC 32/89" },
		{ "name": "18(1)" },
		{ "name": "18(4)A" },
		{ "name": "34(1)" },
		{ "name": "15(1)" },
		{ "name": "3(1)(a)" },
		{ "name": "3(1)(b)" },
		{ "name": "7(1)(a)" },
		{ "name": "7(1)(b)" },
		{ "name": "7(1)(c)" },
		{ "name": "18(1)(a)" },
		{ "name": "18(1)(b)" },
		{ "name": "26(2)(a)" },
		{ "name": "26(2)(b)" },
		{ "name": "26(3)(a)" },
		{ "name": "26(3)(b)" },
		{ "name": "16(b)" },
		{ "name": "16(c)" },
		{ "name": "38(b)" },
		{ "name": "2(a)" },
		{ "name": "21(1)(b)(1)(b)" },
		{ "name": "29(c)" },
		{ "name": "29(d)" },
		{ "name": "21(1)(a)" },
		{ "name": "20(d)" },
		{ "name": "29(a)" },
		{ "name": "29(e)(1)" },
		{ "name": "29(e)(3)" },
		{ "name": "21(a)" },
		{ "name": "21(d)" },
		{ "name": "8(1)(a)" },
		{ "name": "8(1)(b)" },
		{ "name": "8(1)(c)" },
		{ "name": "51(1)(1)" },
		{ "name": "35(1)" },
		{ "name": "35(1)(c)" },
		{ "name": "9(1)(a) " },
		{ "name": "9(1)(b) " },
		{ "name": "9(1)(c)" },
		{ "name": "19(1)(a)" },
		{ "name": "13" },
		{ "name": "2-12" },
		{ "name": "2-15" },
		{ "name": "TSP-6" },
		{ "name": "34(4)" },
		{ "name": "22(a)" },
		{ "name": "22(1)(a)" },
		{ "name": "22(1)(b)" },
		{ "name": "18(4)(b)" },
		{ "name": "21(2)" },
		{ "name": "21(4)(b)" },
		{ "name": "34(4)(a)" },
		{ "name": "21(2)(30)3" },
		{ "name": "25(1)A" },
		{ "name": "18(4)B" },
		{ "name": "20-(1) A,B" },
		{ "name": "21-(1) B" },
		{ "name": "21(1)4" },
		{ "name": "97" },
		{ "name": "19-(1) B" },
		{ "name": "26-3" },
		{ "name": "3(2) A" },
		{ "name": "20-1-C" },
		{ "name": "31-1" },
		{ "name": "25-1" },
		{ "name": "15-3-7-A" },
		{ "name": "15-4-A" },
		{ "name": "20-2-A" },
		{ "name": "4-1" },
		{ "name": "2/39/3(1)" },
		{ "name": "29(h)(4)" },
		{ "name": "20(1)(a)" },
		{ "name": "20(1) j a" },
		{ "name": "20(1)(a) j c" },
		{ "name": "section 5" },
		{ "name": "4 and 30(3)" },
		{ "name": "4 and 8 (1) c" },
		{ "name": "9(1)d" },
		{ "name": "20(a)1" },
		{ "name": "20(1)b" },
		{ "name": "21(4)a" },
		{ "name": "22(1) and 22(2)(a)" },
		{ "name": "30(2)a" },
		{ "name": "34(1)b" },
		{ "name": "34(4)b" },
		{ "name": "29(e) 2" },
		{ "name": "21(1)(a), 30(30)" },
		{ "name": "52(1)a" },
		{ "name": "29(b), 30(3)" },
		{ "name": "29(g)" },
		{ "name": "20(1)b" },
		{ "name": "30(1)a" },
		{ "name": "34(1)b" },
		{ "name": "04 paragraph" },
		{ "name": "27(4)" },
		{ "name": "21(4)a" },
		{ "name": "5(21)" },
		{ "name": "22(1)c" },
		{ "name": "789/12" },
		{ "name": "29(c),30(3)" },
		{ "name": "798/12" },
		{ "name": "10-1a" },
		{ "name": "2-4" },
		{ "name": "section 12" },
		{ "name": "section 16(1)" },
		{ "name": "18(02)" },
		{ "name": "4(1) and 7(c)" },
		{ "name": "3(7) (b)" },
		{ "name": "4(1) and 7(1)" },
		{ "name": "87" },
		{ "name": "4(2)" },
		{ "name": "10(1)b" },
		{ "name": "2(1)a" },
		{ "name": "24(1)" },
		{ "name": "19(3)" }
	]

	constructor(private _formBuilder: FormBuilder,
		private router: Router,
		public pensionerService: PensionerService,
		public dataConfig: DataConfigService,
		private notification: NotificationsService) { }


	ngOnInit() {
		this.cstate = false;
		this.fillpayment.salary_scale = 0;
		this.fillpayment.service_grade = 0;
		this.allowance_description = "";
		this.nonallowance_description = "";
		this.nonallowance_initialamount = 0;
		this.fillpayment = this.pensionerService.fillpaymentmodel;
		this.allarray = this.pensionerService.allowancesarraylist;
		this.nonallarray = this.pensionerService.nonallowancesarraylist;

		this.form = this._formBuilder.group({
			paymentInformation: this._formBuilder.group({
				circular: ['', Validators.required],
				reduced_salary: ['', Validators.required],
				unreduced_salary: ['', Validators.required],
				allowance: ['', Validators.required],
				allowance_amount: ['', Validators.required],
				nonallowance_amountt: ['', Validators.required],
				scale: ['', Validators.required],
				grade: ['', Validators.required],
				salary: ['', Validators.required],
				dateofretire: ['', Validators.required],
				dateofincre: ['', Validators.required],
				servicperiod_years: ['', Validators.required],
				servicperiod_months: ['', Validators.required],
				servicperiod_days: ['', Validators.required],
				nopayperiod_years: ['', Validators.required],
				nopayperiod_months: ['', Validators.required],
				nopayperiod_days: ['', Validators.required],
				totallowances: ['', Validators.required],
				totnonallowances: ['', Validators.required],
				non_allowance: ['', Validators.required],
				nonallowance_amount: ['', Validators.required],
				section: ['', Validators.required],
				reduced_2015: ['', Validators.required],
				unreduced_2015: ['', Validators.required],
				reduced_2017: ['', Validators.required],
				unreduced_2017: ['', Validators.required],
			})
		});

		//Re-calculate if any changes done to the form
		this.form.valueChanges.subscribe(value => {
			this.pensionerService.calculateState = false;
		})

		this.fillpayment.circular_revised = "2017";

		if (this.pensionerService.editstate == false) {
			this.fillpayment.circular = null;
			this.fillpayment.section = null;
			this.fillpayment.salary_scale = null;
			this.fillpayment.service_grade = null;
			this.fillpayment.basic_salary = 0;
			this.fillpayment.nopayperiod_days = 0;
			this.fillpayment.nopayperiod_months = 0;
			this.fillpayment.nopayperiod_years = 0;
			this.fillpayment.servicperiod_days = 0;
			this.fillpayment.servicperiod_months = 0;
			this.fillpayment.servicperiod_years = 0;
			this.fillpayment.allowance_tot = 0;
			this.fillpayment.nonallowance_tot = 0;
		}

		this.setdata();
	}

	ngOnDestroy() {
		this.fillpayment.salary_scale = null;
		this.fillpayment.basic_salary = null;
		this.fillpayment.circular = " ";
		this.fillpayment.service_grade = null;
		this.fillpayment.increment_date = " ";
		this.fillpayment.nopayperiod_days = null;
		this.fillpayment.nopayperiod_months = null;
		this.fillpayment.nopayperiod_years = null;
		this.fillpayment.servicperiod_days = null;
		this.fillpayment.servicperiod_months = null;
		this.fillpayment.servicperiod_years = null;
		this.fillpayment.section = "";
		this.allarray = [];
		this.nonallarray = [];
		this.pensionerService.allowancesarraylist = [];
		this.pensionerService.nonallowancesarraylist = [];
		this.fillpayment.allowance_tot = null;
		this.fillpayment.nonallowance_tot = null;
	}

	setdata() {
		this.pensiondetail = this.pensionerService.pensionermodel;

		if (this.pensionerService.editstate == false) {
			this.fillpayment.increment_date = this.pensiondetail.rdate;
		}
	}

	initFormChnageHooks() {
		console.log(this.form.get);
	}

	//<---Allowances calculation - Start---->
	//Set pensionable allowances
	setallowances() {
		this.pensionerService.allowancesarraylist.push({
			id: this.sequenceNumber,
			description: this.allowance_description,
			amount: this.allowance_amount,
			seqNo: this.sequenceNumber,
			status: "",
			is_pensionable: true
		});

		this.allarray = this.pensionerService.allowancesarraylist;
		this.sequenceNumber += 1;
		this.allowance_description = "";
		this.allowance_amount = 0;
		this.totalAllowances();
	}

	//Remove Pensionable Allowances
	remove_allo(index: number) {
		this.allarray = this.pensionerService.allowancesarraylist.filter(el => el.id != index);
		this.sequenceNumber = this.pensionerService.allowancesarraylist.length + 1;
		for (let i = 0; i < this.pensionerService.allowancesarraylist.length; i++) {
			this.pensionerService.allowancesarraylist[i].id = i - 1;

			var y: number = 0;
			y = this.pensionerService.allowancesarraylist[i].amount;
		}
		this.fillpayment.allowance_tot = this.fillpayment.allowance_tot - y || 0;
		this.pensionerService.allowancesarraylist = [];
	}

	//Set Non pensionable allowances
	setnonallowances() {
		this.pensionerService.nonallowancesarraylist.push({
			id: this.sequenceNumber,
			description: this.nonallowance_description,
			amount: this.nonallowance_initialamount,
			seqNo: this.sequenceNumber,
			status: "",
			is_pensionable: false
		});

		this.nonallarray = this.pensionerService.nonallowancesarraylist;
		this.sequenceNumber += 1;
		this.nonallowance_description = "";
		this.nonallowance_initialamount = 0;
		this.totalNonAllowances();
	}

	//Remove non pensionable allowances
	remove_nonallo(index: number) {
		this.nonallarray = this.pensionerService.nonallowancesarraylist.filter(el => el.id != index);
		for (let i = 0; i < this.pensionerService.nonallowancesarraylist.length; i++) {
			this.pensionerService.nonallowancesarraylist[i].id = i - 1;

			var x: number = 0;
			x = this.pensionerService.nonallowancesarraylist[i].amount;
		}
		this.fillpayment.nonallowance_tot = this.fillpayment.nonallowance_tot - x || 0;
		this.pensionerService.nonallowancesarraylist = [];
	}

	//Total Pensionable Allowances
	totalAllowances() {
		this.fillpayment.allowance_tot = 0;
		this.pensionerService.allowancesarraylist.forEach(element => {
			this.fillpayment.allowance_tot += parseFloat(element.amount);
		})
	}

	//Total non-pensionable Allowances
	totalNonAllowances() {
		this.fillpayment.nonallowance_tot = 0;
		this.pensionerService.nonallowancesarraylist.forEach(element => {
			this.fillpayment.nonallowance_tot += parseFloat(element.amount);
		})
	}

	//<---Allowances calculation - End---->

	//<---- Calculation --->
	getPercentage() {
		//Filter scalename and gradelist for the service
		this.scalelist.forEach(element => {
			if (this.fillpayment.salary_scale == element.id) {
				this.scalename = element.name;
			}
		})

		this.gradelist.forEach(grade => {
			if (this.fillpayment.service_grade == grade.id) {
				this.gradename = grade.name;
			}
		})

		if (this.pensionerService.editstate == false) {
			var dateincrement = this.pensiondetail.rdate;
		} else {
			var dateincrement = this.fillpayment.increment_date;
		}

		//Get the actual bpen 148%
		this.bpenVariance = (this.pensiondetail.bpen * 148) / 100;

		//<-------- REVISED PENSION CALCULATION - PERCENTAGE ----------->
		this.pensionerService.getpensioncalculation(this.scalename, this.gradename,
			this.fillpayment.basic_salary, this.fillpayment.circular, this.pensiondetail.rdate, dateincrement, this.fillpayment.circular_revised)
			.subscribe(data => {
				this.getMilPercentage(data);
			}, error => {
				if (error["error"]["message"] == "Salary you provided seems to be invalid") {
					this.notification.openSnackBar("Salary you provided seems to be INVALID. Please check the salary and Click CALCULATE.!", '', '');
					this.clearFields();
				}
				else if (error["error"]["code"] == 403 || error["error"]["code"] == 404) {

					this.notification.openSnackBar("INVALID DATA. Please Re-Check the Circular , Salary Scale , Grade entered to proceed.!", '', '');
					this.clearFields();
				}
			})
	}

	getMilPercentage(data) {
		//Retreieve BASIC SALARY , FIRST ALLOWANCES, SECOND ALLOWANCES 
		let rev_array = JSON.parse(JSON.stringify(data));
		this.basic_rev = rev_array.basicSalary; //Basic salary for 2017

		//Earn Increment for 2015
		let curentIncrement = rev_array.firstAllowance; //With more decimal points for the calculation
		this.fillpayment.increment2015 = (curentIncrement).toFixed(2);
		this.fixcurrentincre = (curentIncrement / 12.0).toFixed(2); //toFixed(2)

		//Earn increment for 2017
		let revIncrement = rev_array.secondAllowance;
		this.fillpayment.increment2017 = (revIncrement).toFixed(2);
		this.fixrevincre = (revIncrement / 12.0).toFixed(2);

		let revAnnualConsolidatedSalary = this.basic_rev * 12;
		let revGrossAnnualSalary = revAnnualConsolidatedSalary + revIncrement + this.fillpayment.allowance_tot;

		let militoryPensionList: Array<any> = [3, 40, 41, 43, 44, 45, 60, 63, 69]

		militoryPensionList.forEach(pt => {
			if (this.pensionerService.pensionermodel.pt == pt) {
				this.ptSorted = pt;
			}
		});

		if (this.pensionerService.pensionermodel.pt == this.ptSorted) {
			this.offeredTo = "regular"; //Regular or disabled

			if (this.scalename.includes("or")) {
				this.type = "milcom";
			} else {
				this.type = "milnoncom";
			}

			//<-------- 2017 CALCULATION MILITORY----------->
			this.pensionerService.getpercentagemilitory(this.type, this.fillpayment.servicperiod_months,
				revAnnualConsolidatedSalary, this.fillpayment.circular, this.fillpayment.servicperiod_years,
				this.offeredTo)
				.subscribe(data => {
					//Retreive circular, reduced, unreduced details
					let revPercentageMilitory = JSON.parse(JSON.stringify(data));
					this.rev_reduce_perc = revPercentageMilitory.reduced;
					this.rev_unreduce_perc = revPercentageMilitory.unreduced;
				}, error => {
					this.notification.openSnackBar(error["error"]["message"], '', '');
				})

			//<--------- 2006 CALCULATION MILITORY-------------> 
			this.current_annual_consolidated_salary = this.fillpayment.basic_salary * 12;
			this.cuurent_gross_salary = (this.current_annual_consolidated_salary + curentIncrement + this.fillpayment.allowance_tot);

			this.pensionerService.getpercentagemilitory(this.type, this.fillpayment.servicperiod_months,
				this.current_annual_consolidated_salary, this.fillpayment.circular,
				this.fillpayment.servicperiod_years, this.offeredTo)
				.subscribe(data => {
					this.percentage = [JSON.parse(JSON.stringify(data))];
					this.reduced_perc = this.percentage[0].reduced;
					this.unreduced_perc = this.percentage[0].unreduced;
				})
		}

		this.notification.openSnackBar("View Percentage details ! NOW PRESS CALCULATE BUTTON !", '', '');
	}

	getFinalCal() {

		this.cstate = true;

		if (this.pensionerService.editstate == false) {
			var dateincrement = this.pensiondetail.rdate;
		} else {
			var dateincrement = this.fillpayment.increment_date;
		}

		this.pensionerService.getpensioncalculation(this.scalename, this.gradename, this.fillpayment.basic_salary,
			this.fillpayment.circular, this.pensiondetail.rdate, dateincrement, this.fillpayment.circular_revised)
			.subscribe(data => {

				let rev_array = JSON.parse(JSON.stringify(data));
				this.basic_rev = rev_array.basicSalary; //Basic salary for 2017

				//Earn Increment for 2015
				let curentIncrement = rev_array.firstAllowance; //With more decimal points for the calculation
				this.fillpayment.increment2015 = (curentIncrement).toFixed(2);
				this.fixcurrentincre = (curentIncrement / 12.0).toFixed(2); //toFixed(2)

				//Earn increment for 2017
				let revIncrement = rev_array.secondAllowance;
				this.fillpayment.increment2017 = (revIncrement).toFixed(2);
				this.fixrevincre = (revIncrement / 12.0).toFixed(2);

				let revAnnualConsolidatedSalary = this.basic_rev * 12;
				let revGrossAnnualSalary = revAnnualConsolidatedSalary + revIncrement + this.fillpayment.allowance_tot;

				//2017 reduced salary Start
				let reducedSal2017 = (revGrossAnnualSalary * this.rev_reduce_perc / 100) / 12;
				this.reducedSal2017Fixed = (reducedSal2017).toFixed(2);

				//2017 unreduced salary
				let rev_unreduce_salary = revGrossAnnualSalary * this.rev_unreduce_perc / 100;
				this.rev_unreduce_salary = rev_unreduce_salary / 12;
				this.fixedunrevreduced = (this.rev_unreduce_salary).toFixed(2);

				//<---Calculate the sal diffrence = Unreduced sal - reduced sal--->
				var today = new Date();
				var dd = String(today.getDate()).padStart(2, '0');
				var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
				var yyyy = today.getFullYear();
				let today1 = yyyy + '-' + mm + '-' + dd;
				this.todayfix = parseInt(today1.slice(0, 4));

				//<-- If retired date is more than 2017-01-01 prompt a message  START--->
				let incre_date = this.fillpayment.increment_date;
				this.rdatenew = this.pensiondetail.rdate;
				var myDate = new Date(this.rdatenew);
				let result = myDate.getTime();

				var myDate1 = new Date("2017-01-01");
				let result1 = myDate1.getTime();

				if (result > result1) {
					this.notification.openSnackBar("Invalid retired Date", '', '');
				}

				//<--------- 2006 CALCULATION MILITORY-------------> 
				this.current_annual_consolidated_salary = this.fillpayment.basic_salary * 12;
				this.cuurent_gross_salary = (this.current_annual_consolidated_salary + curentIncrement + this.fillpayment.allowance_tot);

				//20006 - reduced salary 
				let reduced_salary = this.cuurent_gross_salary * this.reduced_perc / 100;
				this.reducedCal = reduced_salary / 12;
				this.fixreducedsal = (this.reducedCal).toFixed(2);
				//2006 - unreduced salary
				let unreduced_salary = this.cuurent_gross_salary * this.unreduced_perc / 100;
				this.unreducedCal = unreduced_salary / 12;
				this.fixunreducedsal = (this.unreducedCal).toFixed(2);

				//<---Calculate sal diffrence = 2015 unreduced sal - 2015 reduced sal-->
				this.gap = this.fixunreducedsal - this.fixreducedsal;

				//<---Calculate new reduced salary--->
				//2017 reduced salary
				let v = this.fixedunrevreduced - this.gap
				this.fixedrevreduced = v.toFixed(2);

				this.date2 = new Date(this.rdatenew);
				this.date1 = new Date();
				var x = this.date1 - this.date2;
				this.diff = new Date(x);
				let years = this.diff.toISOString().slice(0, 4) - 1970;
				let months = this.diff.getMonth();
				let days = this.diff.getDate() - 1;

				let widowOnlyList: Array<any> = [40, 41, 43, 44, 45, 60, 63];
				widowOnlyList.forEach(pt => {
					if (this.pensionerService.pensionermodel.pt == pt) {
						this.widowList = pt;
					}
				});

				if (this.pensionerService.pensionermodel.pt == this.widowList) {
					this.totalpensiontobepaid = (parseFloat(this.rev_unreduce_salary + "") + parseInt(this.fillpayment.nonallowance_tot + "") +
						parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
					// this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
					this.calculateSalaryIncrease();
					this.pensionerService.bpenPension = this.rev_unreduce_salary;
				} else {


					if (years <= 10) {
						if (years == 10) {
							if (months == 0 && days == 0) {
								this.totalpensiontobepaid = (parseFloat(this.fixedrevreduced + "") + parseFloat(this.fillpayment.nonallowance_tot + "") +
									parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
								// this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
								this.calculateSalaryIncrease();
								this.pensionerService.bpenPension = this.reducedSal2017Fixed;
							} else if (months > 0 || days > 0) {
								this.totalpensiontobepaid = (parseFloat(this.rev_unreduce_salary + "") + parseFloat(this.fillpayment.nonallowance_tot + "") +
									parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
								// this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
								this.calculateSalaryIncrease();
								this.pensionerService.bpenPension = this.rev_unreduce_salary;
							}
						} else if (years < 10) {
							this.totalpensiontobepaid = (parseFloat(this.fixedrevreduced + "") + parseFloat(this.fillpayment.nonallowance_tot + "") +
								parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
							// this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
							this.calculateSalaryIncrease();
							this.pensionerService.bpenPension = this.reducedSal2017Fixed;
						}
					} else if (years > 10) {
						this.totalpensiontobepaid = (parseFloat(this.rev_unreduce_salary + "") + parseFloat(this.fillpayment.nonallowance_tot + "") +
							parseInt(this.pensionerService.claFixedAmount + "")).toFixed(2);
						// this.salaryincrease = (this.totalpensiontobepaid - this.pensiondetail.tpen).toFixed(2);
						this.calculateSalaryIncrease();
						this.pensionerService.bpenPension = this.rev_unreduce_salary;
					}

				}
			})

		this.pensionerService.calculateState = true;
	}

	calculateSalaryIncrease() {
		if (this.pensiondetail.cla == 3675) {
			this.salaryincrease = (this.totalpensiontobepaid - 3525 - (this.pensiondetail.bpen + this.pensiondetail.ota + 150)).toFixed(2);
		} else {
			this.salaryincrease = (this.totalpensiontobepaid - 3525 - (this.pensiondetail.bpen + this.pensiondetail.ota)).toFixed(2);
		}
	}

	clearFields() {
		this.reduced_perc = "";
		this.unreduced_perc = "";
		this.fillpayment.increment2017 = null;
		this.fixreducedsal = "";
		this.fixunreducedsal = "";
		this.basic_rev = "";
		this.rev_reduce_perc = "";
		this.rev_unreduce_perc = "";
		this.fillpayment.increment2015 = null;
		this.fixedrevreduced = "";
		this.fixedunrevreduced = "";
		this.totalpensiontobepaid = "";
		this.salaryincrease = "";
	}

}
