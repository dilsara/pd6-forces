import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPaymentInfoComponent } from './view-payment-info.component';

describe('ViewPaymentInfoComponent', () => {
  let component: ViewPaymentInfoComponent;
  let fixture: ComponentFixture<ViewPaymentInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPaymentInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPaymentInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
