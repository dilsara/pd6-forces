import { Component, OnInit, Input } from '@angular/core';
import { DashboardComponent } from 'src/app/layout/dashboard/dashboard.component';
import PD6DashboardConfig from './pd6-config/pd6-config.dashboard';

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

@Component({
	selector: 'app-pd6-dashboard',
	templateUrl: './pd6-dashboard.component.html',
	styleUrls: ['./pd6-dashboard.component.scss']
})
export class Pd6DashboardComponent implements OnInit {

	// counts : [];
	dashboardConfig: PD6DashboardConfig = new PD6DashboardConfig();

	constructor(
		// private dashboardtile: DashboardTileComponent

	) { }

	ngOnInit() {
		// localStorage.setItem("role", "DS_PENSION_OFFICER");
		// localStorage.setItem("username", "testpo3");
		// localStorage.setItem("role","DS_ACCOUNTANT");
		// localStorage.setItem("username","testdsa3");
		this.dashboardConfig.getDashboardConfig(localStorage.getItem("role"));
	}
}
