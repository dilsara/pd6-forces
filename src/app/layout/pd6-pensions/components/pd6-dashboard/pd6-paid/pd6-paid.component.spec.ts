import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd6PaidComponent } from './pd6-paid.component';

describe('Pd6PaidComponent', () => {
  let component: Pd6PaidComponent;
  let fixture: ComponentFixture<Pd6PaidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd6PaidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd6PaidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
