import { Component, OnInit } from '@angular/core';
import { PensionerService } from '../../../services/pensioner.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { RerevisionPopupComponent } from '../../popups/rerevision-popup/rerevision-popup.component';

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

@Component({
	selector: 'app-pd6-paid',
	templateUrl: './pd6-paid.component.html',
	styleUrls: ['./pd6-paid.component.scss']
})
export class Pd6PaidComponent implements OnInit {

	paymentdetails: any = []; nic: any;
	final: any; districtarray: any; submisisonRef: any;
	userRoleOff: any; userRoleAcc: any;

	constructor(private pensionerservice: PensionerService,
		private router: Router,
		private dialog: MatDialog) { }

	ngOnInit() {
		let username = localStorage.getItem("username");
		let userrole = localStorage.getItem("role");

		if (userrole == "DS_PENSION_OFFICER" || userrole == "DS_ACCOUNTANT") {
			this.pensionerservice.get_list(username, 400).subscribe(data => {
				this.paymentdetails = JSON.parse(JSON.stringify(data));
				this.nic = this.paymentdetails.nic;
			})
		} else if (userrole == "DISTRICT_OFFICER") {
			this.pensionerservice.getDashboardListDO(400).subscribe(data => {
				this.paymentdetails = JSON.parse(JSON.stringify(data));
				this.nic = this.paymentdetails.nic;
			})
		}

		if (userrole == "DS_ACCOUNTANT") {
			this.userRoleAcc = userrole;
		} else if (userrole == "DS_PENSION_OFFICER") {
			this.userRoleOff = userrole;
		}
	}

	//View source document
	details(refNumber: string, status: string) {
		this.pensionerservice.stateChanges = "paid";
		localStorage.setItem("ref", refNumber);
		this.router.navigateByUrl('/login/pd6-pensions/pd6-sourcedoc/' + status);
	}

	//Search bar : using NIC
	searchnic() {
		var input, filter, table, tr, td, i, txtValue;
		input = document.getElementById("nic");
		filter = input.value.toUpperCase();
		table = document.getElementById("myTable");
		tr = table.getElementsByTagName("tr");
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[1];
			if (td) {
				txtValue = td.textContent || td.innerText;
				if (txtValue.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			}
		}
	}

	//View award
	view_award(refnumber: string) {
		localStorage.setItem("ref", refnumber);
		this.router.navigateByUrl('/login/pd6-pensions/award/' + 400);
	}

	//Apply fro re-revision
	reRevision(refNumber: string) {
		localStorage.setItem("ref", refNumber);
		this.rerevisionPopup();
	}

	rerevisionPopup() {
		const dialogRef = this.dialog.open(RerevisionPopupComponent, {
			width: '650px',
			height: '300px',
			data: { refNumber: this.submisisonRef }
		});
		dialogRef.afterClosed().subscribe(result => {
		});
	}

}
