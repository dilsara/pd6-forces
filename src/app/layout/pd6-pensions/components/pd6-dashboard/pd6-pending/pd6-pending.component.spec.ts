import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd6PendingComponent } from './pd6-pending.component';

describe('Pd6PendingComponent', () => {
  let component: Pd6PendingComponent;
  let fixture: ComponentFixture<Pd6PendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd6PendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd6PendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
