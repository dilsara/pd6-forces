import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd6DashboardComponent } from './pd6-dashboard.component';

describe('Pd6DashboardComponent', () => {
  let component: Pd6DashboardComponent;
  let fixture: ComponentFixture<Pd6DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd6DashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd6DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
