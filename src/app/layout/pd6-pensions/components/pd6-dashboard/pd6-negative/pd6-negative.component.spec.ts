import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pd6NegativeComponent } from './pd6-negative.component';

describe('Pd6NegativeComponent', () => {
  let component: Pd6NegativeComponent;
  let fixture: ComponentFixture<Pd6NegativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pd6NegativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pd6NegativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
