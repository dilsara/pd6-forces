import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { PensionerService } from '../../../services/pensioner.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router } from '@angular/router';

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

@Component({
  selector: 'app-rerevision-popup',
  templateUrl: './rerevision-popup.component.html',
  styleUrls: ['./rerevision-popup.component.scss']
})
export class RerevisionPopupComponent implements OnInit {
  reason: any; data1: any;

  constructor(public dialogRef: MatDialogRef<RerevisionPopupComponent>,
    private dialog: MatDialog,
    private pensionerservice: PensionerService,
    private notification: NotificationsService,
    private router: Router) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }

  submitReasonRe() {
    let refnumber = localStorage.getItem("ref");
    let values = {
      revisionId: refnumber,
      reason: this.reason
    }

    if (this.reason = null || this.reason == "" || this.reason == undefined) {
      this.notification.openSnackBar("Provide a valid REASON, before proceeding !", '', '');
      this.reason = ""
    } else {
      this.pensionerservice.saveReRevisionReason(values).subscribe(data => {
        let user = localStorage.getItem("username");
        let statusValues = {
          id: parseInt(refnumber),
          status: 101,
          reson: "Re-revised application",
          user: user
        }

        this.pensionerservice.change_status(statusValues).subscribe(data => {
          this.data1 = JSON.parse(JSON.stringify(data));
          if (this.data1.code == 200) {
            this.notification.openSnackBar("Application is sent for RE-REVSION ! ", '', '');
          }
        })
        this.close();
        this.router.navigateByUrl("/login/pd6-pensions");
      })
    }
  }

}
