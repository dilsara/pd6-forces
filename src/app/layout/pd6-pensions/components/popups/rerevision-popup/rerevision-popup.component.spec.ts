import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RerevisionPopupComponent } from './rerevision-popup.component';

describe('RerevisionPopupComponent', () => {
  let component: RerevisionPopupComponent;
  let fixture: ComponentFixture<RerevisionPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RerevisionPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RerevisionPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
