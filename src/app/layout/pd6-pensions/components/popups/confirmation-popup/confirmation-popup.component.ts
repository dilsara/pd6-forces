import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { PensionModel } from 'src/app/layout/pd6-pensions/models/pension.model';
import { Pd6DetailedViewComponent } from '../../pd6-detailed-view/pd6-detailed-view.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

@Component({
	selector: 'app-confirmation-popup',
	templateUrl: './confirmation-popup.component.html',
	styleUrls: ['./confirmation-popup.component.scss']
})
export class ConfirmationPopupComponent implements OnInit {

	public pensionmodel: PensionModel;
	array = [];
	refnumber = null;

	constructor(@Inject(MAT_DIALOG_DATA) public data,
		public dialogRef: MatDialogRef<ConfirmationPopupComponent>,
		private router: Router, ) {
		dialogRef.disableClose = true;
	}

	ngOnInit() {
		this.pensionmodel;
		this.proceed();
	}

	proceed() {
		this.refnumber = this.data.refnumber;
	}

	printsource() {
		this.close();
		this.router.navigateByUrl('/login/pd6-pensions/pd6-sourcedoc/' + 100);
	}

	close(): void {
		this.dialogRef.close();
	}


}
