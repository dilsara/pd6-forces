import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbovebpenPopupComponent } from './abovebpen-popup.component';

describe('AbovebpenPopupComponent', () => {
  let component: AbovebpenPopupComponent;
  let fixture: ComponentFixture<AbovebpenPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbovebpenPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbovebpenPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
