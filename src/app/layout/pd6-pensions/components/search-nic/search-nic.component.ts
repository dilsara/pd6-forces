import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PensionerService } from '../../services/pensioner.service';
import { PensionerModel } from '../../models/pensioner.model';
import { MatCalendarBody } from '@angular/material';
import { stringify } from 'querystring';
import { NotificationsService } from 'src/app/services/notifications.service';

/**
 * subpaths
 * @author Shageesha Prabagaran
 */

@Component({
	selector: 'app-search-nic',
	templateUrl: './search-nic.component.html',
	styleUrls: ['./search-nic.component.scss']
})
export class SearchNicComponent implements OnInit {

	nic: string;
	pensionerList: any = [];
	pensionermodel: PensionerModel;
	activeDes: any; active: any;
	personalInfo: any;
	pensionerAlreadyList: any;

	public activeArrayList: Array<any> = [16, 3, 2, 12, 8, 5, 10, 4, 14, 17, 15, 7, 20, 11, 13]


	constructor(
		private router: Router,
		private pensionerService: PensionerService,
		private notification: NotificationsService
	) {
		this.nic = "";
	}

	ngOnInit() {
	}

	searchpd6Pensioner() {
		this.pensionerList = [];
		let username = localStorage.getItem("username");

		this.pensionerService.get_nic_details(this.nic).subscribe(data => {
			let output = JSON.parse(JSON.stringify(data));
			if (output.status == 0) {
				this.notification.openSnackBar("NIC NUMBER IS INCORRECT", '', '');
			} else if (output.status == 1) { //Already saved NIC - 2 Pensions
				this.pensionerService.alreadyExists = true;
				this.getPensionerDetails();
			} else if (output.status == 2) {
				this.notification.openSnackBar("Same NIC NUMBER IS FOUND - PLEASE CHECK", '', '');
			} else if (output.status == 3) { //New NIC
				//Check for dscode
				this.pensionerService.check_dscode(username, this.nic).subscribe(data => {
					let dscode_result = JSON.parse(JSON.stringify(data));
					if (dscode_result.validForDscode == true) {
						//Get data from 141 DB					
						this.getPensionerDetails();
					} else if (dscode_result.validForDscode == false) {
						this.notification.openSnackBar("Invalid pensioner for dscode", '', '');
					}
				})
			}
		})
	}

	more(pensioner) {
		this.pensionerService.checkNicAvailabilty(this.nic).subscribe(data => {
			let result = JSON.parse(JSON.stringify(data));
			if (result.available) {
				result.pensionTypes.forEach(element => {
					if (pensioner.pt != element) {
						if (pensioner.active == 1) {
							this.pensionerService.editstate = false;
							if (this.pensionerService.alreadyExists) {
								this.pensionerService.getSavedPersonalInfo(this.nic, pensioner.pt).subscribe(data => {
									this.pensionerAlreadyList = JSON.parse(JSON.stringify(data));
									this.pensionerService.pensionermodel = this.pensionerAlreadyList;
									this.pensionerService.personId = this.pensionerAlreadyList.personId;
									this.pensionerService.pt = this.pensionerAlreadyList.pt;
									this.router.navigateByUrl("/login/pd6-pensions/pd6-detailed-view/" + 100);
								})
								return true;
							} else if (!this.pensionerService.alreadyExists) {
								this.pensionerService.pensionermodel = pensioner;
								this.router.navigateByUrl("/login/pd6-pensions/pd6-detailed-view/" + 100);
							}
						} else {
							this.notification.openSnackBar("CANNOT PROCEED - HOLD PENSIONER", '', '');
						}
					}
					else {
						this.notification.openSnackBar("Revision for this pension already exists", '', '');
					}
				});
			}
			else if (!result.available) {
				this.pensionerService.pensionermodel = pensioner;
				if (this.pensionerService.pensionermodel.active == 1) {
					this.pensionerService.editstate = false;
					this.router.navigateByUrl("/login/pd6-pensions/pd6-detailed-view/" + 100);
				}
			}
		})

	}

	//Get data from 141 DB
	getPensionerDetails() {
		this.pensionerService.searchpd6PensionerByNic(this.nic).subscribe(data => {
			this.pensionerList = data["data"];
		}, error => {
			this.notification.openSnackBar(error["error"]["message"], '', '');
		})
	}

}
