import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchNicComponent } from './search-nic.component';

describe('SearchNicComponent', () => {
  let component: SearchNicComponent;
  let fixture: ComponentFixture<SearchNicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchNicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchNicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
