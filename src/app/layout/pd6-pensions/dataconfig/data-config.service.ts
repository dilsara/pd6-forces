import { Injectable } from '@angular/core';
import { PensionModel } from '../models/pension.model';

@Injectable({
  providedIn: 'root'
})
export class DataConfigService {

  pd6SendPensions: Array<PensionModel> = new Array<PensionModel>();

  constructor() { }
}
