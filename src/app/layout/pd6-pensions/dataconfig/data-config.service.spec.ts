import { TestBed } from '@angular/core/testing';

import { DataConfigService } from './data-config.service';

describe('DataConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataConfigService = TestBed.get(DataConfigService);
    expect(service).toBeTruthy();
  });
});
